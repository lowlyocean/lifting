import React, { Component } from 'react';
import { PersistGate } from 'redux-persist/integration/react'
import { Provider } from 'react-redux';
// Imports: Redux Persist Persister
import { store, persistor } from './redux/store/store';
import { Platform, ToastAndroid } from 'react-native';
import { AdMobBanner } from 'expo-ads-admob'
import Constants from 'expo-constants'
import Lifting from './Lifting';

export default class App extends Component {
  render() {
    const adUnitID = !Constants.isDevice ? "ca-app-pub-3940256099942544/6300978111" : Platform.OS == "ios" ? "ca-app-pub-7249523127344802/3000699098" : "ca-app-pub-7249523127344802/5760925503";
    return (
    // Redux: Global Store
    <Provider store={store}>
      <PersistGate loading={null} persistor={persistor}>      
        <Lifting />
        <AdMobBanner
          style={{ position: "relative", bottom: 0 }}
          bannerSize="fullBanner"
          adUnitID={adUnitID}
          onDidFailToReceiveAdWithError={error => Platform.OS === "android" && ToastAndroid.show(error, ToastAndroid.LONG)}
        />
      </PersistGate>
    </Provider>
    );
  }
}
