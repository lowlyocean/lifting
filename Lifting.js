import { connect } from 'react-redux';

import React, { Component } from 'react';
import { Button, ScrollView, StyleSheet, StatusBar, View } from 'react-native';
import regression from 'regression';
const HtmlTableToJson = require('html-table-to-json');
import html from './constants';

import t from 'tcomb-form-native';

// Imports: Redux Actions
import { calculated } from './redux/actions/calculatedActions';
import { setInputs } from './redux/actions/inputsActions';
import { setResults } from './redux/actions/resultsActions';

const Form = t.form.Form;

const exercises = [
  'bench-press',
  'bent-over-row',
  'lat-pulldown',
  'deadlift',
  'squat',
  'shoulder-press'
];

var Gender = t.enums({
  M: 'Male',
  F: 'Female'
}, 'Gender');

const Results = t.struct(exercises.reduce((o, key) => ({ ...o, [key]: t.Number}), {}));

const RepCalc = t.struct({
  gender: Gender,
  body_weight: t.Number,
  reps: t.Number,
  start_date: t.Date,
  workout_date: t.Date,
});

const options = {
  fields: {
    start_date: {
      mode: 'date',
      config: {
        dialogMode: 'calendar', // or spinner
        format: (date) => date.toLocaleDateString()
      }
    },
    workout_date: {
      mode: 'date',
      config: {
        dialogMode: 'calendar', // or spinner
        format: (date) => date.toLocaleDateString()
      }
    },
  },
};

const lift = (orm, reps) => {
  return orm*(37-reps)/36
}

const styles = StyleSheet.create({
  container: {
    flex:1,
    justifyContent: 'center',
    marginTop: StatusBar.currentHeight,
    padding: 20,
    backgroundColor: '#ffffff',
  },
});

const zip = (a, b) => a.map((k, i) => [k, b[i]]);

let formulaFromRow = function (row) {
  let pointsLessThanThirty = zip([30,180,730,1825],[row['Beg.'], row['Nov.'], row['Int.'],row['Adv.'],row['Elite']].map(v => Number.parseInt(v.substr(0,v.indexOf(' '))))); // for < 30days
  let coeffLessThanThirty = regression.power(pointsLessThanThirty).equation;
  let pointsGreaterThanThirty = zip([180,730,1825].map((orig)=> orig-30), [row['Nov.'], row['Int.'],row['Adv.'],row['Elite']].map(v => Number.parseInt(v.substr(0,v.indexOf(' ')))).map((orig)=> orig-pointsLessThanThirty[0][1])); // for >= 30days
  let coeffGreaterThanThirty = regression.power(pointsGreaterThanThirty).equation;
  return (elapse) => elapse < 30 ? coeffLessThanThirty[0] * elapse ** coeffLessThanThirty[1] : pointsLessThanThirty[0][1] + coeffGreaterThanThirty[0] * (elapse - 30) ** coeffGreaterThanThirty[1]
}

const findNearestBodyweight = (objects,target) => {
  return objects.sort(function(a,b){ 
    return Math.abs(a.BW-target) - Math.abs(b.BW-target)
    })[0];
}

const getStandardsFormula = (gender, bodyweight, exercise) => {
  const rawData = HtmlTableToJson.parse(html[exercise]).results[gender == 'M' ? 0 : 1];
  return formulaFromRow(findNearestBodyweight(rawData, bodyweight));
}

const getStandardsFormulas = (gender, bodyweight) => {
  return exercises.reduce((o, key) => ({ ...o, [key]: getStandardsFormula(gender, bodyweight, key)}), {})
}

const getResults = (inputs) => {
  const elapse = Number.parseInt((inputs.workout_date - inputs.start_date)/ (1000 * 60 * 60 * 24)); //ms to days
  const standardsFormulas = getStandardsFormulas(inputs.gender, inputs.body_weight);
  return(exercises.reduce((o, key) => ({ ...o, [key]: Number.parseInt(lift(standardsFormulas[key](elapse), inputs.reps))}), {}))
}

class Lifting extends Component {

    handleSubmit = () => {
        this.props.reduxCalculated(false);
        this.props.reduxSetResults(getResults(this.props.inputs));
        this.props.reduxCalculated(true);
    }

  render() {
    return (
        <View style={styles.container}>
            <ScrollView contentContainerStyle={{flexGrow: 1, justifyContent: 'center'}} >
                <Form
                ref={c => this._form = c} // assign a ref
                type={RepCalc} 
                options={options}
                value={this.props.inputs}
                onChange={(value) => { this.props.reduxSetInputs(value); this.props.reduxCalculated(false) } } />
                <Button
                title="What should I lift?"
                onPress={this.handleSubmit}
                />
            {this.props.calculated && 
                <Form
                type={Results}
                value={this.props.results}
                options={{fields: exercises.reduce((o, key) => ({ ...o, [key]: {editable: false}}), {})}}
                />
            }
            </ScrollView>
        </View>
    );
  }
}

// Map State To Props (Redux Store Passes State To Component)
const mapStateToProps = (state) => {
    // Redux Store --> Component
    return {
      inputs: state.inputsReducer.inputs,
      calculated: state.calculatedReducer.calculated,
      results: state.resultsReducer.results
    };
  };
  
  // Map Dispatch To Props (Dispatch Actions To Reducers. Reducers Then Modify The Data And Assign It To Your Props)
  const mapDispatchToProps = (dispatch) => {
    // Action
    return {
      // Set Inputs
      reduxSetInputs: (value) => dispatch(setInputs(value)),
      // Toggle Calculated
      reduxCalculated: (trueFalse) => dispatch(calculated(trueFalse)),
      // Set Results
      reduxSetResults: (value) => dispatch(setResults(value))
    };
  };
  // Exports
  export default connect(mapStateToProps, mapDispatchToProps)(Lifting);