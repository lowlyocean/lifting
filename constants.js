// constants.es6
export default {
      'bench-press':
      `
      <table class="table is-fullwidth is-striped is-narrow standards__table" id="tableId601a19aef11b3">
    <thead>
    <tr>
      <th style="width: 25%;">
        <abbr title="Bodyweight"><span>BW</span></abbr>
      </th>
                  <th style="width: 15%;">
            <abbr
                title="Beginner"><span>Beg.</span></abbr>
          </th>
                  <th style="width: 15%;">
            <abbr
                title="Novice"><span>Nov.</span></abbr>
          </th>
                  <th style="width: 15%;">
            <abbr
                title="Intermediate"><span>Int.</span></abbr>
          </th>
                  <th style="width: 15%;">
            <abbr
                title="Advanced"><span>Adv.</span></abbr>
          </th>
                  <th style="width: 15%;">
            <abbr
                title="Elite"><span>Elite</span></abbr>
          </th>
            </tr>
    </thead>
    <tbody>
          <tr>
                      <td>
                110                                  <span class="standards__ratio"></span>
                            </td>
                      <td>
                51                                  <span class="standards__ratio">x0.46</span>
                            </td>
                      <td>
                82                                  <span class="standards__ratio">x0.74</span>
                            </td>
                      <td>
                122                                  <span class="standards__ratio">x1.11</span>
                            </td>
                      <td>
                170                                  <span class="standards__ratio">x1.55</span>
                            </td>
                      <td>
                223                                  <span class="standards__ratio">x2.03</span>
                            </td>
                </tr>
          <tr>
                      <td>
                120                                  <span class="standards__ratio"></span>
                            </td>
                      <td>
                61                                  <span class="standards__ratio">x0.51</span>
                            </td>
                      <td>
                94                                  <span class="standards__ratio">x0.79</span>
                            </td>
                      <td>
                137                                  <span class="standards__ratio">x1.14</span>
                            </td>
                      <td>
                188                                  <span class="standards__ratio">x1.56</span>
                            </td>
                      <td>
                243                                  <span class="standards__ratio">x2.03</span>
                            </td>
                </tr>
          <tr>
                      <td>
                130                                  <span class="standards__ratio"></span>
                            </td>
                      <td>
                71                                  <span class="standards__ratio">x0.54</span>
                            </td>
                      <td>
                106                                  <span class="standards__ratio">x0.82</span>
                            </td>
                      <td>
                151                                  <span class="standards__ratio">x1.17</span>
                            </td>
                      <td>
                205                                  <span class="standards__ratio">x1.57</span>
                            </td>
                      <td>
                263                                  <span class="standards__ratio">x2.02</span>
                            </td>
                </tr>
          <tr>
                      <td>
                140                                  <span class="standards__ratio"></span>
                            </td>
                      <td>
                80                                  <span class="standards__ratio">x0.57</span>
                            </td>
                      <td>
                118                                  <span class="standards__ratio">x0.84</span>
                            </td>
                      <td>
                166                                  <span class="standards__ratio">x1.18</span>
                            </td>
                      <td>
                221                                  <span class="standards__ratio">x1.58</span>
                            </td>
                      <td>
                281                                  <span class="standards__ratio">x2.01</span>
                            </td>
                </tr>
          <tr>
                      <td>
                150                                  <span class="standards__ratio"></span>
                            </td>
                      <td>
                90                                  <span class="standards__ratio">x0.6</span>
                            </td>
                      <td>
                130                                  <span class="standards__ratio">x0.87</span>
                            </td>
                      <td>
                179                                  <span class="standards__ratio">x1.2</span>
                            </td>
                      <td>
                237                                  <span class="standards__ratio">x1.58</span>
                            </td>
                      <td>
                299                                  <span class="standards__ratio">x1.99</span>
                            </td>
                </tr>
          <tr>
                      <td>
                160                                  <span class="standards__ratio"></span>
                            </td>
                      <td>
                100                                  <span class="standards__ratio">x0.62</span>
                            </td>
                      <td>
                141                                  <span class="standards__ratio">x0.88</span>
                            </td>
                      <td>
                193                                  <span class="standards__ratio">x1.2</span>
                            </td>
                      <td>
                252                                  <span class="standards__ratio">x1.57</span>
                            </td>
                      <td>
                316                                  <span class="standards__ratio">x1.98</span>
                            </td>
                </tr>
          <tr>
                      <td>
                170                                  <span class="standards__ratio"></span>
                            </td>
                      <td>
                109                                  <span class="standards__ratio">x0.64</span>
                            </td>
                      <td>
                152                                  <span class="standards__ratio">x0.9</span>
                            </td>
                      <td>
                206                                  <span class="standards__ratio">x1.21</span>
                            </td>
                      <td>
                267                                  <span class="standards__ratio">x1.57</span>
                            </td>
                      <td>
                333                                  <span class="standards__ratio">x1.96</span>
                            </td>
                </tr>
          <tr>
                      <td>
                180                                  <span class="standards__ratio"></span>
                            </td>
                      <td>
                118                                  <span class="standards__ratio">x0.66</span>
                            </td>
                      <td>
                163                                  <span class="standards__ratio">x0.91</span>
                            </td>
                      <td>
                218                                  <span class="standards__ratio">x1.21</span>
                            </td>
                      <td>
                281                                  <span class="standards__ratio">x1.56</span>
                            </td>
                      <td>
                348                                  <span class="standards__ratio">x1.94</span>
                            </td>
                </tr>
          <tr>
                      <td>
                190                                  <span class="standards__ratio"></span>
                            </td>
                      <td>
                127                                  <span class="standards__ratio">x0.67</span>
                            </td>
                      <td>
                174                                  <span class="standards__ratio">x0.91</span>
                            </td>
                      <td>
                230                                  <span class="standards__ratio">x1.21</span>
                            </td>
                      <td>
                295                                  <span class="standards__ratio">x1.55</span>
                            </td>
                      <td>
                364                                  <span class="standards__ratio">x1.92</span>
                            </td>
                </tr>
          <tr>
                      <td>
                200                                  <span class="standards__ratio"></span>
                            </td>
                      <td>
                136                                  <span class="standards__ratio">x0.68</span>
                            </td>
                      <td>
                184                                  <span class="standards__ratio">x0.92</span>
                            </td>
                      <td>
                242                                  <span class="standards__ratio">x1.21</span>
                            </td>
                      <td>
                308                                  <span class="standards__ratio">x1.54</span>
                            </td>
                      <td>
                379                                  <span class="standards__ratio">x1.89</span>
                            </td>
                </tr>
          <tr>
                      <td>
                210                                  <span class="standards__ratio"></span>
                            </td>
                      <td>
                145                                  <span class="standards__ratio">x0.69</span>
                            </td>
                      <td>
                194                                  <span class="standards__ratio">x0.93</span>
                            </td>
                      <td>
                254                                  <span class="standards__ratio">x1.21</span>
                            </td>
                      <td>
                321                                  <span class="standards__ratio">x1.53</span>
                            </td>
                      <td>
                393                                  <span class="standards__ratio">x1.87</span>
                            </td>
                </tr>
          <tr>
                      <td>
                220                                  <span class="standards__ratio"></span>
                            </td>
                      <td>
                153                                  <span class="standards__ratio">x0.7</span>
                            </td>
                      <td>
                204                                  <span class="standards__ratio">x0.93</span>
                            </td>
                      <td>
                265                                  <span class="standards__ratio">x1.21</span>
                            </td>
                      <td>
                334                                  <span class="standards__ratio">x1.52</span>
                            </td>
                      <td>
                407                                  <span class="standards__ratio">x1.85</span>
                            </td>
                </tr>
          <tr>
                      <td>
                230                                  <span class="standards__ratio"></span>
                            </td>
                      <td>
                162                                  <span class="standards__ratio">x0.7</span>
                            </td>
                      <td>
                214                                  <span class="standards__ratio">x0.93</span>
                            </td>
                      <td>
                276                                  <span class="standards__ratio">x1.2</span>
                            </td>
                      <td>
                347                                  <span class="standards__ratio">x1.51</span>
                            </td>
                      <td>
                421                                  <span class="standards__ratio">x1.83</span>
                            </td>
                </tr>
          <tr>
                      <td>
                240                                  <span class="standards__ratio"></span>
                            </td>
                      <td>
                170                                  <span class="standards__ratio">x0.71</span>
                            </td>
                      <td>
                223                                  <span class="standards__ratio">x0.93</span>
                            </td>
                      <td>
                287                                  <span class="standards__ratio">x1.2</span>
                            </td>
                      <td>
                359                                  <span class="standards__ratio">x1.49</span>
                            </td>
                      <td>
                434                                  <span class="standards__ratio">x1.81</span>
                            </td>
                </tr>
          <tr>
                      <td>
                250                                  <span class="standards__ratio"></span>
                            </td>
                      <td>
                178                                  <span class="standards__ratio">x0.71</span>
                            </td>
                      <td>
                233                                  <span class="standards__ratio">x0.93</span>
                            </td>
                      <td>
                298                                  <span class="standards__ratio">x1.19</span>
                            </td>
                      <td>
                370                                  <span class="standards__ratio">x1.48</span>
                            </td>
                      <td>
                447                                  <span class="standards__ratio">x1.79</span>
                            </td>
                </tr>
          <tr>
                      <td>
                260                                  <span class="standards__ratio"></span>
                            </td>
                      <td>
                186                                  <span class="standards__ratio">x0.72</span>
                            </td>
                      <td>
                242                                  <span class="standards__ratio">x0.93</span>
                            </td>
                      <td>
                308                                  <span class="standards__ratio">x1.18</span>
                            </td>
                      <td>
                382                                  <span class="standards__ratio">x1.47</span>
                            </td>
                      <td>
                460                                  <span class="standards__ratio">x1.77</span>
                            </td>
                </tr>
          <tr>
                      <td>
                270                                  <span class="standards__ratio"></span>
                            </td>
                      <td>
                194                                  <span class="standards__ratio">x0.72</span>
                            </td>
                      <td>
                251                                  <span class="standards__ratio">x0.93</span>
                            </td>
                      <td>
                318                                  <span class="standards__ratio">x1.18</span>
                            </td>
                      <td>
                393                                  <span class="standards__ratio">x1.46</span>
                            </td>
                      <td>
                472                                  <span class="standards__ratio">x1.75</span>
                            </td>
                </tr>
          <tr>
                      <td>
                280                                  <span class="standards__ratio"></span>
                            </td>
                      <td>
                202                                  <span class="standards__ratio">x0.72</span>
                            </td>
                      <td>
                260                                  <span class="standards__ratio">x0.93</span>
                            </td>
                      <td>
                328                                  <span class="standards__ratio">x1.17</span>
                            </td>
                      <td>
                404                                  <span class="standards__ratio">x1.44</span>
                            </td>
                      <td>
                484                                  <span class="standards__ratio">x1.73</span>
                            </td>
                </tr>
          <tr>
                      <td>
                290                                  <span class="standards__ratio"></span>
                            </td>
                      <td>
                210                                  <span class="standards__ratio">x0.72</span>
                            </td>
                      <td>
                268                                  <span class="standards__ratio">x0.93</span>
                            </td>
                      <td>
                338                                  <span class="standards__ratio">x1.16</span>
                            </td>
                      <td>
                415                                  <span class="standards__ratio">x1.43</span>
                            </td>
                      <td>
                496                                  <span class="standards__ratio">x1.71</span>
                            </td>
                </tr>
          <tr>
                      <td>
                300                                  <span class="standards__ratio"></span>
                            </td>
                      <td>
                217                                  <span class="standards__ratio">x0.72</span>
                            </td>
                      <td>
                277                                  <span class="standards__ratio">x0.92</span>
                            </td>
                      <td>
                347                                  <span class="standards__ratio">x1.16</span>
                            </td>
                      <td>
                425                                  <span class="standards__ratio">x1.42</span>
                            </td>
                      <td>
                508                                  <span class="standards__ratio">x1.69</span>
                            </td>
                </tr>
          <tr>
                      <td>
                310                                  <span class="standards__ratio"></span>
                            </td>
                      <td>
                224                                  <span class="standards__ratio">x0.72</span>
                            </td>
                      <td>
                285                                  <span class="standards__ratio">x0.92</span>
                            </td>
                      <td>
                356                                  <span class="standards__ratio">x1.15</span>
                            </td>
                      <td>
                436                                  <span class="standards__ratio">x1.41</span>
                            </td>
                      <td>
                519                                  <span class="standards__ratio">x1.67</span>
                            </td>
                </tr>
        </tbody>
              <tfoot class="standards__footer">
        <tr>
                          <td>
                  All              </td>
                          <td>
                  101              </td>
                          <td>
                  151              </td>
                          <td>
                  214              </td>
                          <td>
                  288              </td>
                          <td>
                  369              </td>
                    </tr>
        </tfoot>
        </table>
        <table class="table is-fullwidth is-striped is-narrow standards__table" id="tableId601a19aef12c6">
        <thead>
        <tr>
          <th style="width: 25%;">
            <abbr title="Bodyweight"><span>BW</span></abbr>
          </th>
                      <th style="width: 15%;">
                <abbr
                    title="Beginner"><span>Beg.</span></abbr>
              </th>
                      <th style="width: 15%;">
                <abbr
                    title="Novice"><span>Nov.</span></abbr>
              </th>
                      <th style="width: 15%;">
                <abbr
                    title="Intermediate"><span>Int.</span></abbr>
              </th>
                      <th style="width: 15%;">
                <abbr
                    title="Advanced"><span>Adv.</span></abbr>
              </th>
                      <th style="width: 15%;">
                <abbr
                    title="Elite"><span>Elite</span></abbr>
              </th>
                </tr>
        </thead>
        <tbody>
              <tr>
                          <td>
                    90                                  <span class="standards__ratio"></span>
                                </td>
                          <td>
                    19                                  <span class="standards__ratio">x0.21</span>
                                </td>
                          <td>
                    40                                  <span class="standards__ratio">x0.44</span>
                                </td>
                          <td>
                    70                                  <span class="standards__ratio">x0.78</span>
                                </td>
                          <td>
                    109                                  <span class="standards__ratio">x1.21</span>
                                </td>
                          <td>
                    154                                  <span class="standards__ratio">x1.71</span>
                                </td>
                    </tr>
              <tr>
                          <td>
                    100                                  <span class="standards__ratio"></span>
                                </td>
                          <td>
                    23                                  <span class="standards__ratio">x0.23</span>
                                </td>
                          <td>
                    46                                  <span class="standards__ratio">x0.46</span>
                                </td>
                          <td>
                    78                                  <span class="standards__ratio">x0.78</span>
                                </td>
                          <td>
                    119                                  <span class="standards__ratio">x1.19</span>
                                </td>
                          <td>
                    166                                  <span class="standards__ratio">x1.66</span>
                                </td>
                    </tr>
              <tr>
                          <td>
                    110                                  <span class="standards__ratio"></span>
                                </td>
                          <td>
                    27                                  <span class="standards__ratio">x0.25</span>
                                </td>
                          <td>
                    52                                  <span class="standards__ratio">x0.47</span>
                                </td>
                          <td>
                    86                                  <span class="standards__ratio">x0.78</span>
                                </td>
                          <td>
                    128                                  <span class="standards__ratio">x1.17</span>
                                </td>
                          <td>
                    177                                  <span class="standards__ratio">x1.61</span>
                                </td>
                    </tr>
              <tr>
                          <td>
                    120                                  <span class="standards__ratio"></span>
                                </td>
                          <td>
                    32                                  <span class="standards__ratio">x0.26</span>
                                </td>
                          <td>
                    58                                  <span class="standards__ratio">x0.48</span>
                                </td>
                          <td>
                    93                                  <span class="standards__ratio">x0.78</span>
                                </td>
                          <td>
                    137                                  <span class="standards__ratio">x1.14</span>
                                </td>
                          <td>
                    187                                  <span class="standards__ratio">x1.56</span>
                                </td>
                    </tr>
              <tr>
                          <td>
                    130                                  <span class="standards__ratio"></span>
                                </td>
                          <td>
                    36                                  <span class="standards__ratio">x0.27</span>
                                </td>
                          <td>
                    63                                  <span class="standards__ratio">x0.49</span>
                                </td>
                          <td>
                    100                                  <span class="standards__ratio">x0.77</span>
                                </td>
                          <td>
                    145                                  <span class="standards__ratio">x1.12</span>
                                </td>
                          <td>
                    197                                  <span class="standards__ratio">x1.51</span>
                                </td>
                    </tr>
              <tr>
                          <td>
                    140                                  <span class="standards__ratio"></span>
                                </td>
                          <td>
                    40                                  <span class="standards__ratio">x0.28</span>
                                </td>
                          <td>
                    68                                  <span class="standards__ratio">x0.49</span>
                                </td>
                          <td>
                    107                                  <span class="standards__ratio">x0.76</span>
                                </td>
                          <td>
                    153                                  <span class="standards__ratio">x1.1</span>
                                </td>
                          <td>
                    206                                  <span class="standards__ratio">x1.47</span>
                                </td>
                    </tr>
              <tr>
                          <td>
                    150                                  <span class="standards__ratio"></span>
                                </td>
                          <td>
                    44                                  <span class="standards__ratio">x0.29</span>
                                </td>
                          <td>
                    73                                  <span class="standards__ratio">x0.49</span>
                                </td>
                          <td>
                    113                                  <span class="standards__ratio">x0.75</span>
                                </td>
                          <td>
                    161                                  <span class="standards__ratio">x1.07</span>
                                </td>
                          <td>
                    215                                  <span class="standards__ratio">x1.43</span>
                                </td>
                    </tr>
              <tr>
                          <td>
                    160                                  <span class="standards__ratio"></span>
                                </td>
                          <td>
                    47                                  <span class="standards__ratio">x0.3</span>
                                </td>
                          <td>
                    78                                  <span class="standards__ratio">x0.49</span>
                                </td>
                          <td>
                    119                                  <span class="standards__ratio">x0.74</span>
                                </td>
                          <td>
                    168                                  <span class="standards__ratio">x1.05</span>
                                </td>
                          <td>
                    223                                  <span class="standards__ratio">x1.39</span>
                                </td>
                    </tr>
              <tr>
                          <td>
                    170                                  <span class="standards__ratio"></span>
                                </td>
                          <td>
                    51                                  <span class="standards__ratio">x0.3</span>
                                </td>
                          <td>
                    83                                  <span class="standards__ratio">x0.49</span>
                                </td>
                          <td>
                    125                                  <span class="standards__ratio">x0.74</span>
                                </td>
                          <td>
                    175                                  <span class="standards__ratio">x1.03</span>
                                </td>
                          <td>
                    231                                  <span class="standards__ratio">x1.36</span>
                                </td>
                    </tr>
              <tr>
                          <td>
                    180                                  <span class="standards__ratio"></span>
                                </td>
                          <td>
                    55                                  <span class="standards__ratio">x0.3</span>
                                </td>
                          <td>
                    88                                  <span class="standards__ratio">x0.49</span>
                                </td>
                          <td>
                    131                                  <span class="standards__ratio">x0.73</span>
                                </td>
                          <td>
                    182                                  <span class="standards__ratio">x1.01</span>
                                </td>
                          <td>
                    239                                  <span class="standards__ratio">x1.33</span>
                                </td>
                    </tr>
              <tr>
                          <td>
                    190                                  <span class="standards__ratio"></span>
                                </td>
                          <td>
                    58                                  <span class="standards__ratio">x0.31</span>
                                </td>
                          <td>
                    92                                  <span class="standards__ratio">x0.49</span>
                                </td>
                          <td>
                    136                                  <span class="standards__ratio">x0.72</span>
                                </td>
                          <td>
                    188                                  <span class="standards__ratio">x0.99</span>
                                </td>
                          <td>
                    246                                  <span class="standards__ratio">x1.3</span>
                                </td>
                    </tr>
              <tr>
                          <td>
                    200                                  <span class="standards__ratio"></span>
                                </td>
                          <td>
                    62                                  <span class="standards__ratio">x0.31</span>
                                </td>
                          <td>
                    97                                  <span class="standards__ratio">x0.48</span>
                                </td>
                          <td>
                    141                                  <span class="standards__ratio">x0.71</span>
                                </td>
                          <td>
                    195                                  <span class="standards__ratio">x0.97</span>
                                </td>
                          <td>
                    253                                  <span class="standards__ratio">x1.27</span>
                                </td>
                    </tr>
              <tr>
                          <td>
                    210                                  <span class="standards__ratio"></span>
                                </td>
                          <td>
                    65                                  <span class="standards__ratio">x0.31</span>
                                </td>
                          <td>
                    101                                  <span class="standards__ratio">x0.48</span>
                                </td>
                          <td>
                    147                                  <span class="standards__ratio">x0.7</span>
                                </td>
                          <td>
                    201                                  <span class="standards__ratio">x0.96</span>
                                </td>
                          <td>
                    260                                  <span class="standards__ratio">x1.24</span>
                                </td>
                    </tr>
              <tr>
                          <td>
                    220                                  <span class="standards__ratio"></span>
                                </td>
                          <td>
                    69                                  <span class="standards__ratio">x0.31</span>
                                </td>
                          <td>
                    105                                  <span class="standards__ratio">x0.48</span>
                                </td>
                          <td>
                    152                                  <span class="standards__ratio">x0.69</span>
                                </td>
                          <td>
                    207                                  <span class="standards__ratio">x0.94</span>
                                </td>
                          <td>
                    267                                  <span class="standards__ratio">x1.21</span>
                                </td>
                    </tr>
              <tr>
                          <td>
                    230                                  <span class="standards__ratio"></span>
                                </td>
                          <td>
                    72                                  <span class="standards__ratio">x0.31</span>
                                </td>
                          <td>
                    109                                  <span class="standards__ratio">x0.47</span>
                                </td>
                          <td>
                    156                                  <span class="standards__ratio">x0.68</span>
                                </td>
                          <td>
                    212                                  <span class="standards__ratio">x0.92</span>
                                </td>
                          <td>
                    273                                  <span class="standards__ratio">x1.19</span>
                                </td>
                    </tr>
              <tr>
                          <td>
                    240                                  <span class="standards__ratio"></span>
                                </td>
                          <td>
                    75                                  <span class="standards__ratio">x0.31</span>
                                </td>
                          <td>
                    113                                  <span class="standards__ratio">x0.47</span>
                                </td>
                          <td>
                    161                                  <span class="standards__ratio">x0.67</span>
                                </td>
                          <td>
                    218                                  <span class="standards__ratio">x0.91</span>
                                </td>
                          <td>
                    280                                  <span class="standards__ratio">x1.16</span>
                                </td>
                    </tr>
              <tr>
                          <td>
                    250                                  <span class="standards__ratio"></span>
                                </td>
                          <td>
                    78                                  <span class="standards__ratio">x0.31</span>
                                </td>
                          <td>
                    117                                  <span class="standards__ratio">x0.47</span>
                                </td>
                          <td>
                    166                                  <span class="standards__ratio">x0.66</span>
                                </td>
                          <td>
                    223                                  <span class="standards__ratio">x0.89</span>
                                </td>
                          <td>
                    286                                  <span class="standards__ratio">x1.14</span>
                                </td>
                    </tr>
              <tr>
                          <td>
                    260                                  <span class="standards__ratio"></span>
                                </td>
                          <td>
                    81                                  <span class="standards__ratio">x0.31</span>
                                </td>
                          <td>
                    121                                  <span class="standards__ratio">x0.46</span>
                                </td>
                          <td>
                    170                                  <span class="standards__ratio">x0.65</span>
                                </td>
                          <td>
                    228                                  <span class="standards__ratio">x0.88</span>
                                </td>
                          <td>
                    291                                  <span class="standards__ratio">x1.12</span>
                                </td>
                    </tr>
            </tbody>
                  <tfoot class="standards__footer">
            <tr>
                              <td>
                      All              </td>
                              <td>
                      38              </td>
                              <td>
                      69              </td>
                              <td>
                      110              </td>
                              <td>
                      162              </td>
                              <td>
                      219              </td>
                        </tr>
            </tfoot>
            </table>
      `,
      'bent-over-row':
      `
      <table class="table is-fullwidth is-striped is-narrow standards__table" id="tableId601a1c088fa58">
    <thead>
    <tr>
      <th style="width: 25%;">
        <abbr title="Bodyweight"><span>BW</span></abbr>
      </th>
                  <th style="width: 15%;">
            <abbr
                title="Beginner"><span>Beg.</span></abbr>
          </th>
                  <th style="width: 15%;">
            <abbr
                title="Novice"><span>Nov.</span></abbr>
          </th>
                  <th style="width: 15%;">
            <abbr
                title="Intermediate"><span>Int.</span></abbr>
          </th>
                  <th style="width: 15%;">
            <abbr
                title="Advanced"><span>Adv.</span></abbr>
          </th>
                  <th style="width: 15%;">
            <abbr
                title="Elite"><span>Elite</span></abbr>
          </th>
            </tr>
    </thead>
    <tbody>
          <tr>
                      <td>
                110                                  <span class="standards__ratio"></span>
                            </td>
                      <td>
                45                                  <span class="standards__ratio">x0.41</span>
                            </td>
                      <td>
                74                                  <span class="standards__ratio">x0.67</span>
                            </td>
                      <td>
                111                                  <span class="standards__ratio">x1.01</span>
                            </td>
                      <td>
                156                                  <span class="standards__ratio">x1.41</span>
                            </td>
                      <td>
                205                                  <span class="standards__ratio">x1.87</span>
                            </td>
                </tr>
          <tr>
                      <td>
                120                                  <span class="standards__ratio"></span>
                            </td>
                      <td>
                54                                  <span class="standards__ratio">x0.45</span>
                            </td>
                      <td>
                84                                  <span class="standards__ratio">x0.7</span>
                            </td>
                      <td>
                124                                  <span class="standards__ratio">x1.03</span>
                            </td>
                      <td>
                171                                  <span class="standards__ratio">x1.42</span>
                            </td>
                      <td>
                222                                  <span class="standards__ratio">x1.85</span>
                            </td>
                </tr>
          <tr>
                      <td>
                130                                  <span class="standards__ratio"></span>
                            </td>
                      <td>
                62                                  <span class="standards__ratio">x0.48</span>
                            </td>
                      <td>
                94                                  <span class="standards__ratio">x0.73</span>
                            </td>
                      <td>
                136                                  <span class="standards__ratio">x1.05</span>
                            </td>
                      <td>
                185                                  <span class="standards__ratio">x1.42</span>
                            </td>
                      <td>
                239                                  <span class="standards__ratio">x1.84</span>
                            </td>
                </tr>
          <tr>
                      <td>
                140                                  <span class="standards__ratio"></span>
                            </td>
                      <td>
                70                                  <span class="standards__ratio">x0.5</span>
                            </td>
                      <td>
                104                                  <span class="standards__ratio">x0.75</span>
                            </td>
                      <td>
                148                                  <span class="standards__ratio">x1.06</span>
                            </td>
                      <td>
                199                                  <span class="standards__ratio">x1.42</span>
                            </td>
                      <td>
                254                                  <span class="standards__ratio">x1.82</span>
                            </td>
                </tr>
          <tr>
                      <td>
                150                                  <span class="standards__ratio"></span>
                            </td>
                      <td>
                78                                  <span class="standards__ratio">x0.52</span>
                            </td>
                      <td>
                114                                  <span class="standards__ratio">x0.76</span>
                            </td>
                      <td>
                159                                  <span class="standards__ratio">x1.06</span>
                            </td>
                      <td>
                212                                  <span class="standards__ratio">x1.41</span>
                            </td>
                      <td>
                270                                  <span class="standards__ratio">x1.8</span>
                            </td>
                </tr>
          <tr>
                      <td>
                160                                  <span class="standards__ratio"></span>
                            </td>
                      <td>
                86                                  <span class="standards__ratio">x0.54</span>
                            </td>
                      <td>
                124                                  <span class="standards__ratio">x0.77</span>
                            </td>
                      <td>
                170                                  <span class="standards__ratio">x1.07</span>
                            </td>
                      <td>
                225                                  <span class="standards__ratio">x1.41</span>
                            </td>
                      <td>
                284                                  <span class="standards__ratio">x1.77</span>
                            </td>
                </tr>
          <tr>
                      <td>
                170                                  <span class="standards__ratio"></span>
                            </td>
                      <td>
                94                                  <span class="standards__ratio">x0.55</span>
                            </td>
                      <td>
                133                                  <span class="standards__ratio">x0.78</span>
                            </td>
                      <td>
                181                                  <span class="standards__ratio">x1.07</span>
                            </td>
                      <td>
                237                                  <span class="standards__ratio">x1.4</span>
                            </td>
                      <td>
                298                                  <span class="standards__ratio">x1.75</span>
                            </td>
                </tr>
          <tr>
                      <td>
                180                                  <span class="standards__ratio"></span>
                            </td>
                      <td>
                101                                  <span class="standards__ratio">x0.56</span>
                            </td>
                      <td>
                142                                  <span class="standards__ratio">x0.79</span>
                            </td>
                      <td>
                192                                  <span class="standards__ratio">x1.07</span>
                            </td>
                      <td>
                249                                  <span class="standards__ratio">x1.39</span>
                            </td>
                      <td>
                311                                  <span class="standards__ratio">x1.73</span>
                            </td>
                </tr>
          <tr>
                      <td>
                190                                  <span class="standards__ratio"></span>
                            </td>
                      <td>
                109                                  <span class="standards__ratio">x0.57</span>
                            </td>
                      <td>
                151                                  <span class="standards__ratio">x0.79</span>
                            </td>
                      <td>
                202                                  <span class="standards__ratio">x1.06</span>
                            </td>
                      <td>
                261                                  <span class="standards__ratio">x1.37</span>
                            </td>
                      <td>
                324                                  <span class="standards__ratio">x1.71</span>
                            </td>
                </tr>
          <tr>
                      <td>
                200                                  <span class="standards__ratio"></span>
                            </td>
                      <td>
                116                                  <span class="standards__ratio">x0.58</span>
                            </td>
                      <td>
                159                                  <span class="standards__ratio">x0.8</span>
                            </td>
                      <td>
                212                                  <span class="standards__ratio">x1.06</span>
                            </td>
                      <td>
                272                                  <span class="standards__ratio">x1.36</span>
                            </td>
                      <td>
                337                                  <span class="standards__ratio">x1.68</span>
                            </td>
                </tr>
          <tr>
                      <td>
                210                                  <span class="standards__ratio"></span>
                            </td>
                      <td>
                123                                  <span class="standards__ratio">x0.59</span>
                            </td>
                      <td>
                168                                  <span class="standards__ratio">x0.8</span>
                            </td>
                      <td>
                222                                  <span class="standards__ratio">x1.06</span>
                            </td>
                      <td>
                283                                  <span class="standards__ratio">x1.35</span>
                            </td>
                      <td>
                349                                  <span class="standards__ratio">x1.66</span>
                            </td>
                </tr>
          <tr>
                      <td>
                220                                  <span class="standards__ratio"></span>
                            </td>
                      <td>
                130                                  <span class="standards__ratio">x0.59</span>
                            </td>
                      <td>
                176                                  <span class="standards__ratio">x0.8</span>
                            </td>
                      <td>
                231                                  <span class="standards__ratio">x1.05</span>
                            </td>
                      <td>
                294                                  <span class="standards__ratio">x1.34</span>
                            </td>
                      <td>
                361                                  <span class="standards__ratio">x1.64</span>
                            </td>
                </tr>
          <tr>
                      <td>
                230                                  <span class="standards__ratio"></span>
                            </td>
                      <td>
                137                                  <span class="standards__ratio">x0.6</span>
                            </td>
                      <td>
                184                                  <span class="standards__ratio">x0.8</span>
                            </td>
                      <td>
                240                                  <span class="standards__ratio">x1.04</span>
                            </td>
                      <td>
                304                                  <span class="standards__ratio">x1.32</span>
                            </td>
                      <td>
                372                                  <span class="standards__ratio">x1.62</span>
                            </td>
                </tr>
          <tr>
                      <td>
                240                                  <span class="standards__ratio"></span>
                            </td>
                      <td>
                144                                  <span class="standards__ratio">x0.6</span>
                            </td>
                      <td>
                192                                  <span class="standards__ratio">x0.8</span>
                            </td>
                      <td>
                249                                  <span class="standards__ratio">x1.04</span>
                            </td>
                      <td>
                314                                  <span class="standards__ratio">x1.31</span>
                            </td>
                      <td>
                383                                  <span class="standards__ratio">x1.6</span>
                            </td>
                </tr>
          <tr>
                      <td>
                250                                  <span class="standards__ratio"></span>
                            </td>
                      <td>
                151                                  <span class="standards__ratio">x0.6</span>
                            </td>
                      <td>
                199                                  <span class="standards__ratio">x0.8</span>
                            </td>
                      <td>
                258                                  <span class="standards__ratio">x1.03</span>
                            </td>
                      <td>
                324                                  <span class="standards__ratio">x1.3</span>
                            </td>
                      <td>
                394                                  <span class="standards__ratio">x1.58</span>
                            </td>
                </tr>
          <tr>
                      <td>
                260                                  <span class="standards__ratio"></span>
                            </td>
                      <td>
                157                                  <span class="standards__ratio">x0.6</span>
                            </td>
                      <td>
                207                                  <span class="standards__ratio">x0.8</span>
                            </td>
                      <td>
                267                                  <span class="standards__ratio">x1.03</span>
                            </td>
                      <td>
                334                                  <span class="standards__ratio">x1.28</span>
                            </td>
                      <td>
                405                                  <span class="standards__ratio">x1.56</span>
                            </td>
                </tr>
          <tr>
                      <td>
                270                                  <span class="standards__ratio"></span>
                            </td>
                      <td>
                164                                  <span class="standards__ratio">x0.61</span>
                            </td>
                      <td>
                214                                  <span class="standards__ratio">x0.79</span>
                            </td>
                      <td>
                275                                  <span class="standards__ratio">x1.02</span>
                            </td>
                      <td>
                343                                  <span class="standards__ratio">x1.27</span>
                            </td>
                      <td>
                415                                  <span class="standards__ratio">x1.54</span>
                            </td>
                </tr>
          <tr>
                      <td>
                280                                  <span class="standards__ratio"></span>
                            </td>
                      <td>
                170                                  <span class="standards__ratio">x0.61</span>
                            </td>
                      <td>
                222                                  <span class="standards__ratio">x0.79</span>
                            </td>
                      <td>
                283                                  <span class="standards__ratio">x1.01</span>
                            </td>
                      <td>
                352                                  <span class="standards__ratio">x1.26</span>
                            </td>
                      <td>
                425                                  <span class="standards__ratio">x1.52</span>
                            </td>
                </tr>
          <tr>
                      <td>
                290                                  <span class="standards__ratio"></span>
                            </td>
                      <td>
                176                                  <span class="standards__ratio">x0.61</span>
                            </td>
                      <td>
                229                                  <span class="standards__ratio">x0.79</span>
                            </td>
                      <td>
                291                                  <span class="standards__ratio">x1</span>
                            </td>
                      <td>
                361                                  <span class="standards__ratio">x1.25</span>
                            </td>
                      <td>
                435                                  <span class="standards__ratio">x1.5</span>
                            </td>
                </tr>
          <tr>
                      <td>
                300                                  <span class="standards__ratio"></span>
                            </td>
                      <td>
                182                                  <span class="standards__ratio">x0.61</span>
                            </td>
                      <td>
                236                                  <span class="standards__ratio">x0.79</span>
                            </td>
                      <td>
                299                                  <span class="standards__ratio">x1</span>
                            </td>
                      <td>
                370                                  <span class="standards__ratio">x1.23</span>
                            </td>
                      <td>
                445                                  <span class="standards__ratio">x1.48</span>
                            </td>
                </tr>
          <tr>
                      <td>
                310                                  <span class="standards__ratio"></span>
                            </td>
                      <td>
                188                                  <span class="standards__ratio">x0.61</span>
                            </td>
                      <td>
                242                                  <span class="standards__ratio">x0.78</span>
                            </td>
                      <td>
                307                                  <span class="standards__ratio">x0.99</span>
                            </td>
                      <td>
                378                                  <span class="standards__ratio">x1.22</span>
                            </td>
                      <td>
                454                                  <span class="standards__ratio">x1.46</span>
                            </td>
                </tr>
        </tbody>
              <tfoot class="standards__footer">
        <tr>
                          <td>
                  All              </td>
                          <td>
                  89              </td>
                          <td>
                  132              </td>
                          <td>
                  187              </td>
                          <td>
                  252              </td>
                          <td>
                  322              </td>
                    </tr>
        </tfoot>
        </table>
        <table class="table is-fullwidth is-striped is-narrow standards__table" id="tableId601a1c088fb74">
    <thead>
    <tr>
      <th style="width: 25%;">
        <abbr title="Bodyweight"><span>BW</span></abbr>
      </th>
                  <th style="width: 15%;">
            <abbr
                title="Beginner"><span>Beg.</span></abbr>
          </th>
                  <th style="width: 15%;">
            <abbr
                title="Novice"><span>Nov.</span></abbr>
          </th>
                  <th style="width: 15%;">
            <abbr
                title="Intermediate"><span>Int.</span></abbr>
          </th>
                  <th style="width: 15%;">
            <abbr
                title="Advanced"><span>Adv.</span></abbr>
          </th>
                  <th style="width: 15%;">
            <abbr
                title="Elite"><span>Elite</span></abbr>
          </th>
            </tr>
    </thead>
    <tbody>
          <tr>
                      <td>
                90                                  <span class="standards__ratio"></span>
                            </td>
                      <td>
                24                                  <span class="standards__ratio">x0.26</span>
                            </td>
                      <td>
                44                                  <span class="standards__ratio">x0.48</span>
                            </td>
                      <td>
                71                                  <span class="standards__ratio">x0.78</span>
                            </td>
                      <td>
                104                                  <span class="standards__ratio">x1.16</span>
                            </td>
                      <td>
                143                                  <span class="standards__ratio">x1.58</span>
                            </td>
                </tr>
          <tr>
                      <td>
                100                                  <span class="standards__ratio"></span>
                            </td>
                      <td>
                26                                  <span class="standards__ratio">x0.26</span>
                            </td>
                      <td>
                47                                  <span class="standards__ratio">x0.47</span>
                            </td>
                      <td>
                75                                  <span class="standards__ratio">x0.75</span>
                            </td>
                      <td>
                110                                  <span class="standards__ratio">x1.1</span>
                            </td>
                      <td>
                149                                  <span class="standards__ratio">x1.49</span>
                            </td>
                </tr>
          <tr>
                      <td>
                110                                  <span class="standards__ratio"></span>
                            </td>
                      <td>
                29                                  <span class="standards__ratio">x0.26</span>
                            </td>
                      <td>
                50                                  <span class="standards__ratio">x0.46</span>
                            </td>
                      <td>
                79                                  <span class="standards__ratio">x0.72</span>
                            </td>
                      <td>
                115                                  <span class="standards__ratio">x1.04</span>
                            </td>
                      <td>
                155                                  <span class="standards__ratio">x1.41</span>
                            </td>
                </tr>
          <tr>
                      <td>
                120                                  <span class="standards__ratio"></span>
                            </td>
                      <td>
                31                                  <span class="standards__ratio">x0.26</span>
                            </td>
                      <td>
                53                                  <span class="standards__ratio">x0.44</span>
                            </td>
                      <td>
                83                                  <span class="standards__ratio">x0.69</span>
                            </td>
                      <td>
                119                                  <span class="standards__ratio">x0.99</span>
                            </td>
                      <td>
                160                                  <span class="standards__ratio">x1.33</span>
                            </td>
                </tr>
          <tr>
                      <td>
                130                                  <span class="standards__ratio"></span>
                            </td>
                      <td>
                33                                  <span class="standards__ratio">x0.26</span>
                            </td>
                      <td>
                56                                  <span class="standards__ratio">x0.43</span>
                            </td>
                      <td>
                87                                  <span class="standards__ratio">x0.67</span>
                            </td>
                      <td>
                124                                  <span class="standards__ratio">x0.95</span>
                            </td>
                      <td>
                165                                  <span class="standards__ratio">x1.27</span>
                            </td>
                </tr>
          <tr>
                      <td>
                140                                  <span class="standards__ratio"></span>
                            </td>
                      <td>
                35                                  <span class="standards__ratio">x0.25</span>
                            </td>
                      <td>
                59                                  <span class="standards__ratio">x0.42</span>
                            </td>
                      <td>
                90                                  <span class="standards__ratio">x0.64</span>
                            </td>
                      <td>
                128                                  <span class="standards__ratio">x0.91</span>
                            </td>
                      <td>
                170                                  <span class="standards__ratio">x1.21</span>
                            </td>
                </tr>
          <tr>
                      <td>
                150                                  <span class="standards__ratio"></span>
                            </td>
                      <td>
                37                                  <span class="standards__ratio">x0.25</span>
                            </td>
                      <td>
                62                                  <span class="standards__ratio">x0.41</span>
                            </td>
                      <td>
                93                                  <span class="standards__ratio">x0.62</span>
                            </td>
                      <td>
                132                                  <span class="standards__ratio">x0.88</span>
                            </td>
                      <td>
                174                                  <span class="standards__ratio">x1.16</span>
                            </td>
                </tr>
          <tr>
                      <td>
                160                                  <span class="standards__ratio"></span>
                            </td>
                      <td>
                39                                  <span class="standards__ratio">x0.25</span>
                            </td>
                      <td>
                64                                  <span class="standards__ratio">x0.4</span>
                            </td>
                      <td>
                96                                  <span class="standards__ratio">x0.6</span>
                            </td>
                      <td>
                135                                  <span class="standards__ratio">x0.84</span>
                            </td>
                      <td>
                178                                  <span class="standards__ratio">x1.11</span>
                            </td>
                </tr>
          <tr>
                      <td>
                170                                  <span class="standards__ratio"></span>
                            </td>
                      <td>
                41                                  <span class="standards__ratio">x0.24</span>
                            </td>
                      <td>
                67                                  <span class="standards__ratio">x0.39</span>
                            </td>
                      <td>
                99                                  <span class="standards__ratio">x0.58</span>
                            </td>
                      <td>
                139                                  <span class="standards__ratio">x0.82</span>
                            </td>
                      <td>
                182                                  <span class="standards__ratio">x1.07</span>
                            </td>
                </tr>
          <tr>
                      <td>
                180                                  <span class="standards__ratio"></span>
                            </td>
                      <td>
                43                                  <span class="standards__ratio">x0.24</span>
                            </td>
                      <td>
                69                                  <span class="standards__ratio">x0.38</span>
                            </td>
                      <td>
                102                                  <span class="standards__ratio">x0.57</span>
                            </td>
                      <td>
                142                                  <span class="standards__ratio">x0.79</span>
                            </td>
                      <td>
                186                                  <span class="standards__ratio">x1.03</span>
                            </td>
                </tr>
          <tr>
                      <td>
                190                                  <span class="standards__ratio"></span>
                            </td>
                      <td>
                45                                  <span class="standards__ratio">x0.24</span>
                            </td>
                      <td>
                71                                  <span class="standards__ratio">x0.37</span>
                            </td>
                      <td>
                105                                  <span class="standards__ratio">x0.55</span>
                            </td>
                      <td>
                145                                  <span class="standards__ratio">x0.76</span>
                            </td>
                      <td>
                190                                  <span class="standards__ratio">x1</span>
                            </td>
                </tr>
          <tr>
                      <td>
                200                                  <span class="standards__ratio"></span>
                            </td>
                      <td>
                47                                  <span class="standards__ratio">x0.23</span>
                            </td>
                      <td>
                73                                  <span class="standards__ratio">x0.37</span>
                            </td>
                      <td>
                107                                  <span class="standards__ratio">x0.54</span>
                            </td>
                      <td>
                148                                  <span class="standards__ratio">x0.74</span>
                            </td>
                      <td>
                193                                  <span class="standards__ratio">x0.97</span>
                            </td>
                </tr>
          <tr>
                      <td>
                210                                  <span class="standards__ratio"></span>
                            </td>
                      <td>
                48                                  <span class="standards__ratio">x0.23</span>
                            </td>
                      <td>
                75                                  <span class="standards__ratio">x0.36</span>
                            </td>
                      <td>
                110                                  <span class="standards__ratio">x0.52</span>
                            </td>
                      <td>
                151                                  <span class="standards__ratio">x0.72</span>
                            </td>
                      <td>
                197                                  <span class="standards__ratio">x0.94</span>
                            </td>
                </tr>
          <tr>
                      <td>
                220                                  <span class="standards__ratio"></span>
                            </td>
                      <td>
                50                                  <span class="standards__ratio">x0.23</span>
                            </td>
                      <td>
                77                                  <span class="standards__ratio">x0.35</span>
                            </td>
                      <td>
                112                                  <span class="standards__ratio">x0.51</span>
                            </td>
                      <td>
                154                                  <span class="standards__ratio">x0.7</span>
                            </td>
                      <td>
                200                                  <span class="standards__ratio">x0.91</span>
                            </td>
                </tr>
          <tr>
                      <td>
                230                                  <span class="standards__ratio"></span>
                            </td>
                      <td>
                51                                  <span class="standards__ratio">x0.22</span>
                            </td>
                      <td>
                79                                  <span class="standards__ratio">x0.34</span>
                            </td>
                      <td>
                115                                  <span class="standards__ratio">x0.5</span>
                            </td>
                      <td>
                157                                  <span class="standards__ratio">x0.68</span>
                            </td>
                      <td>
                203                                  <span class="standards__ratio">x0.88</span>
                            </td>
                </tr>
          <tr>
                      <td>
                240                                  <span class="standards__ratio"></span>
                            </td>
                      <td>
                53                                  <span class="standards__ratio">x0.22</span>
                            </td>
                      <td>
                81                                  <span class="standards__ratio">x0.34</span>
                            </td>
                      <td>
                117                                  <span class="standards__ratio">x0.49</span>
                            </td>
                      <td>
                159                                  <span class="standards__ratio">x0.66</span>
                            </td>
                      <td>
                206                                  <span class="standards__ratio">x0.86</span>
                            </td>
                </tr>
          <tr>
                      <td>
                250                                  <span class="standards__ratio"></span>
                            </td>
                      <td>
                54                                  <span class="standards__ratio">x0.22</span>
                            </td>
                      <td>
                83                                  <span class="standards__ratio">x0.33</span>
                            </td>
                      <td>
                119                                  <span class="standards__ratio">x0.48</span>
                            </td>
                      <td>
                162                                  <span class="standards__ratio">x0.65</span>
                            </td>
                      <td>
                209                                  <span class="standards__ratio">x0.83</span>
                            </td>
                </tr>
          <tr>
                      <td>
                260                                  <span class="standards__ratio"></span>
                            </td>
                      <td>
                56                                  <span class="standards__ratio">x0.21</span>
                            </td>
                      <td>
                85                                  <span class="standards__ratio">x0.33</span>
                            </td>
                      <td>
                121                                  <span class="standards__ratio">x0.47</span>
                            </td>
                      <td>
                164                                  <span class="standards__ratio">x0.63</span>
                            </td>
                      <td>
                212                                  <span class="standards__ratio">x0.81</span>
                            </td>
                </tr>
        </tbody>
              <tfoot class="standards__footer">
        <tr>
                          <td>
                  All              </td>
                          <td>
                  35              </td>
                          <td>
                  59              </td>
                          <td>
                  91              </td>
                          <td>
                  130              </td>
                          <td>
                  173              </td>
                    </tr>
        </tfoot>
        </table>
      `,
      'lat-pulldown':
    ` 
    <table class="table is-fullwidth is-striped is-narrow standards__table" id="tableId601a1f5f8b4bb">
    <thead>
    <tr>
      <th style="width: 25%;">
        <abbr title="Bodyweight"><span>BW</span></abbr>
      </th>
                  <th style="width: 15%;">
            <abbr
                title="Beginner"><span>Beg.</span></abbr>
          </th>
                  <th style="width: 15%;">
            <abbr
                title="Novice"><span>Nov.</span></abbr>
          </th>
                  <th style="width: 15%;">
            <abbr
                title="Intermediate"><span>Int.</span></abbr>
          </th>
                  <th style="width: 15%;">
            <abbr
                title="Advanced"><span>Adv.</span></abbr>
          </th>
                  <th style="width: 15%;">
            <abbr
                title="Elite"><span>Elite</span></abbr>
          </th>
            </tr>
    </thead>
    <tbody>
          <tr>
                      <td>
                110                                  <span class="standards__ratio"></span>
                            </td>
                      <td>
                53                                  <span class="standards__ratio">x0.48</span>
                            </td>
                      <td>
                84                                  <span class="standards__ratio">x0.76</span>
                            </td>
                      <td>
                125                                  <span class="standards__ratio">x1.14</span>
                            </td>
                      <td>
                174                                  <span class="standards__ratio">x1.58</span>
                            </td>
                      <td>
                229                                  <span class="standards__ratio">x2.08</span>
                            </td>
                </tr>
          <tr>
                      <td>
                120                                  <span class="standards__ratio"></span>
                            </td>
                      <td>
                59                                  <span class="standards__ratio">x0.49</span>
                            </td>
                      <td>
                92                                  <span class="standards__ratio">x0.77</span>
                            </td>
                      <td>
                135                                  <span class="standards__ratio">x1.12</span>
                            </td>
                      <td>
                186                                  <span class="standards__ratio">x1.55</span>
                            </td>
                      <td>
                242                                  <span class="standards__ratio">x2.02</span>
                            </td>
                </tr>
          <tr>
                      <td>
                130                                  <span class="standards__ratio"></span>
                            </td>
                      <td>
                65                                  <span class="standards__ratio">x0.5</span>
                            </td>
                      <td>
                100                                  <span class="standards__ratio">x0.77</span>
                            </td>
                      <td>
                144                                  <span class="standards__ratio">x1.11</span>
                            </td>
                      <td>
                197                                  <span class="standards__ratio">x1.51</span>
                            </td>
                      <td>
                254                                  <span class="standards__ratio">x1.96</span>
                            </td>
                </tr>
          <tr>
                      <td>
                140                                  <span class="standards__ratio"></span>
                            </td>
                      <td>
                71                                  <span class="standards__ratio">x0.51</span>
                            </td>
                      <td>
                107                                  <span class="standards__ratio">x0.77</span>
                            </td>
                      <td>
                153                                  <span class="standards__ratio">x1.09</span>
                            </td>
                      <td>
                207                                  <span class="standards__ratio">x1.48</span>
                            </td>
                      <td>
                266                                  <span class="standards__ratio">x1.9</span>
                            </td>
                </tr>
          <tr>
                      <td>
                150                                  <span class="standards__ratio"></span>
                            </td>
                      <td>
                77                                  <span class="standards__ratio">x0.51</span>
                            </td>
                      <td>
                115                                  <span class="standards__ratio">x0.76</span>
                            </td>
                      <td>
                162                                  <span class="standards__ratio">x1.08</span>
                            </td>
                      <td>
                217                                  <span class="standards__ratio">x1.45</span>
                            </td>
                      <td>
                278                                  <span class="standards__ratio">x1.85</span>
                            </td>
                </tr>
          <tr>
                      <td>
                160                                  <span class="standards__ratio"></span>
                            </td>
                      <td>
                83                                  <span class="standards__ratio">x0.52</span>
                            </td>
                      <td>
                122                                  <span class="standards__ratio">x0.76</span>
                            </td>
                      <td>
                170                                  <span class="standards__ratio">x1.06</span>
                            </td>
                      <td>
                227                                  <span class="standards__ratio">x1.42</span>
                            </td>
                      <td>
                288                                  <span class="standards__ratio">x1.8</span>
                            </td>
                </tr>
          <tr>
                      <td>
                170                                  <span class="standards__ratio"></span>
                            </td>
                      <td>
                88                                  <span class="standards__ratio">x0.52</span>
                            </td>
                      <td>
                128                                  <span class="standards__ratio">x0.75</span>
                            </td>
                      <td>
                178                                  <span class="standards__ratio">x1.05</span>
                            </td>
                      <td>
                236                                  <span class="standards__ratio">x1.39</span>
                            </td>
                      <td>
                299                                  <span class="standards__ratio">x1.76</span>
                            </td>
                </tr>
          <tr>
                      <td>
                180                                  <span class="standards__ratio"></span>
                            </td>
                      <td>
                94                                  <span class="standards__ratio">x0.52</span>
                            </td>
                      <td>
                135                                  <span class="standards__ratio">x0.75</span>
                            </td>
                      <td>
                186                                  <span class="standards__ratio">x1.03</span>
                            </td>
                      <td>
                245                                  <span class="standards__ratio">x1.36</span>
                            </td>
                      <td>
                308                                  <span class="standards__ratio">x1.71</span>
                            </td>
                </tr>
          <tr>
                      <td>
                190                                  <span class="standards__ratio"></span>
                            </td>
                      <td>
                99                                  <span class="standards__ratio">x0.52</span>
                            </td>
                      <td>
                141                                  <span class="standards__ratio">x0.74</span>
                            </td>
                      <td>
                193                                  <span class="standards__ratio">x1.02</span>
                            </td>
                      <td>
                253                                  <span class="standards__ratio">x1.33</span>
                            </td>
                      <td>
                318                                  <span class="standards__ratio">x1.67</span>
                            </td>
                </tr>
          <tr>
                      <td>
                200                                  <span class="standards__ratio"></span>
                            </td>
                      <td>
                104                                  <span class="standards__ratio">x0.52</span>
                            </td>
                      <td>
                147                                  <span class="standards__ratio">x0.74</span>
                            </td>
                      <td>
                200                                  <span class="standards__ratio">x1</span>
                            </td>
                      <td>
                261                                  <span class="standards__ratio">x1.31</span>
                            </td>
                      <td>
                327                                  <span class="standards__ratio">x1.63</span>
                            </td>
                </tr>
          <tr>
                      <td>
                210                                  <span class="standards__ratio"></span>
                            </td>
                      <td>
                109                                  <span class="standards__ratio">x0.52</span>
                            </td>
                      <td>
                153                                  <span class="standards__ratio">x0.73</span>
                            </td>
                      <td>
                207                                  <span class="standards__ratio">x0.99</span>
                            </td>
                      <td>
                269                                  <span class="standards__ratio">x1.28</span>
                            </td>
                      <td>
                336                                  <span class="standards__ratio">x1.6</span>
                            </td>
                </tr>
          <tr>
                      <td>
                220                                  <span class="standards__ratio"></span>
                            </td>
                      <td>
                114                                  <span class="standards__ratio">x0.52</span>
                            </td>
                      <td>
                159                                  <span class="standards__ratio">x0.72</span>
                            </td>
                      <td>
                214                                  <span class="standards__ratio">x0.97</span>
                            </td>
                      <td>
                277                                  <span class="standards__ratio">x1.26</span>
                            </td>
                      <td>
                344                                  <span class="standards__ratio">x1.56</span>
                            </td>
                </tr>
          <tr>
                      <td>
                230                                  <span class="standards__ratio"></span>
                            </td>
                      <td>
                119                                  <span class="standards__ratio">x0.52</span>
                            </td>
                      <td>
                164                                  <span class="standards__ratio">x0.71</span>
                            </td>
                      <td>
                220                                  <span class="standards__ratio">x0.96</span>
                            </td>
                      <td>
                284                                  <span class="standards__ratio">x1.23</span>
                            </td>
                      <td>
                352                                  <span class="standards__ratio">x1.53</span>
                            </td>
                </tr>
          <tr>
                      <td>
                240                                  <span class="standards__ratio"></span>
                            </td>
                      <td>
                123                                  <span class="standards__ratio">x0.51</span>
                            </td>
                      <td>
                170                                  <span class="standards__ratio">x0.71</span>
                            </td>
                      <td>
                226                                  <span class="standards__ratio">x0.94</span>
                            </td>
                      <td>
                291                                  <span class="standards__ratio">x1.21</span>
                            </td>
                      <td>
                360                                  <span class="standards__ratio">x1.5</span>
                            </td>
                </tr>
          <tr>
                      <td>
                250                                  <span class="standards__ratio"></span>
                            </td>
                      <td>
                128                                  <span class="standards__ratio">x0.51</span>
                            </td>
                      <td>
                175                                  <span class="standards__ratio">x0.7</span>
                            </td>
                      <td>
                233                                  <span class="standards__ratio">x0.93</span>
                            </td>
                      <td>
                298                                  <span class="standards__ratio">x1.19</span>
                            </td>
                      <td>
                368                                  <span class="standards__ratio">x1.47</span>
                            </td>
                </tr>
          <tr>
                      <td>
                260                                  <span class="standards__ratio"></span>
                            </td>
                      <td>
                132                                  <span class="standards__ratio">x0.51</span>
                            </td>
                      <td>
                180                                  <span class="standards__ratio">x0.69</span>
                            </td>
                      <td>
                238                                  <span class="standards__ratio">x0.92</span>
                            </td>
                      <td>
                305                                  <span class="standards__ratio">x1.17</span>
                            </td>
                      <td>
                376                                  <span class="standards__ratio">x1.44</span>
                            </td>
                </tr>
          <tr>
                      <td>
                270                                  <span class="standards__ratio"></span>
                            </td>
                      <td>
                137                                  <span class="standards__ratio">x0.51</span>
                            </td>
                      <td>
                185                                  <span class="standards__ratio">x0.69</span>
                            </td>
                      <td>
                244                                  <span class="standards__ratio">x0.9</span>
                            </td>
                      <td>
                311                                  <span class="standards__ratio">x1.15</span>
                            </td>
                      <td>
                383                                  <span class="standards__ratio">x1.42</span>
                            </td>
                </tr>
          <tr>
                      <td>
                280                                  <span class="standards__ratio"></span>
                            </td>
                      <td>
                141                                  <span class="standards__ratio">x0.5</span>
                            </td>
                      <td>
                190                                  <span class="standards__ratio">x0.68</span>
                            </td>
                      <td>
                250                                  <span class="standards__ratio">x0.89</span>
                            </td>
                      <td>
                318                                  <span class="standards__ratio">x1.13</span>
                            </td>
                      <td>
                390                                  <span class="standards__ratio">x1.39</span>
                            </td>
                </tr>
          <tr>
                      <td>
                290                                  <span class="standards__ratio"></span>
                            </td>
                      <td>
                145                                  <span class="standards__ratio">x0.5</span>
                            </td>
                      <td>
                195                                  <span class="standards__ratio">x0.67</span>
                            </td>
                      <td>
                255                                  <span class="standards__ratio">x0.88</span>
                            </td>
                      <td>
                324                                  <span class="standards__ratio">x1.12</span>
                            </td>
                      <td>
                397                                  <span class="standards__ratio">x1.37</span>
                            </td>
                </tr>
          <tr>
                      <td>
                300                                  <span class="standards__ratio"></span>
                            </td>
                      <td>
                149                                  <span class="standards__ratio">x0.5</span>
                            </td>
                      <td>
                200                                  <span class="standards__ratio">x0.67</span>
                            </td>
                      <td>
                261                                  <span class="standards__ratio">x0.87</span>
                            </td>
                      <td>
                330                                  <span class="standards__ratio">x1.1</span>
                            </td>
                      <td>
                404                                  <span class="standards__ratio">x1.35</span>
                            </td>
                </tr>
          <tr>
                      <td>
                310                                  <span class="standards__ratio"></span>
                            </td>
                      <td>
                153                                  <span class="standards__ratio">x0.49</span>
                            </td>
                      <td>
                204                                  <span class="standards__ratio">x0.66</span>
                            </td>
                      <td>
                266                                  <span class="standards__ratio">x0.86</span>
                            </td>
                      <td>
                336                                  <span class="standards__ratio">x1.08</span>
                            </td>
                      <td>
                410                                  <span class="standards__ratio">x1.32</span>
                            </td>
                </tr>
        </tbody>
              <tfoot class="standards__footer">
        <tr>
                          <td>
                  All              </td>
                          <td>
                  84              </td>
                          <td>
                  126              </td>
                          <td>
                  179              </td>
                          <td>
                  242              </td>
                          <td>
                  310              </td>
                    </tr>
        </tfoot>
        </table>
            <table class="table is-fullwidth is-striped is-narrow standards__table" id="tableId601a1b4bee9af">
    <thead>
    <tr>
      <th style="width: 25%;">
        <abbr title="Bodyweight"><span>BW</span></abbr>
      </th>
                  <th style="width: 15%;">
            <abbr
                title="Beginner"><span>Beg.</span></abbr>
          </th>
                  <th style="width: 15%;">
            <abbr
                title="Novice"><span>Nov.</span></abbr>
          </th>
                  <th style="width: 15%;">
            <abbr
                title="Intermediate"><span>Int.</span></abbr>
          </th>
                  <th style="width: 15%;">
            <abbr
                title="Advanced"><span>Adv.</span></abbr>
          </th>
                  <th style="width: 15%;">
            <abbr
                title="Elite"><span>Elite</span></abbr>
          </th>
            </tr>
    </thead>
    <tbody>
          <tr>
                      <td>
                90                                  <span class="standards__ratio"></span>
                            </td>
                      <td>
                31                                  <span class="standards__ratio">x0.35</span>
                            </td>
                      <td>
                53                                  <span class="standards__ratio">x0.58</span>
                            </td>
                      <td>
                81                                  <span class="standards__ratio">x0.89</span>
                            </td>
                      <td>
                114                                  <span class="standards__ratio">x1.27</span>
                            </td>
                      <td>
                152                                  <span class="standards__ratio">x1.69</span>
                            </td>
                </tr>
          <tr>
                      <td>
                100                                  <span class="standards__ratio"></span>
                            </td>
                      <td>
                34                                  <span class="standards__ratio">x0.34</span>
                            </td>
                      <td>
                56                                  <span class="standards__ratio">x0.56</span>
                            </td>
                      <td>
                85                                  <span class="standards__ratio">x0.85</span>
                            </td>
                      <td>
                119                                  <span class="standards__ratio">x1.19</span>
                            </td>
                      <td>
                158                                  <span class="standards__ratio">x1.58</span>
                            </td>
                </tr>
          <tr>
                      <td>
                110                                  <span class="standards__ratio"></span>
                            </td>
                      <td>
                37                                  <span class="standards__ratio">x0.33</span>
                            </td>
                      <td>
                59                                  <span class="standards__ratio">x0.54</span>
                            </td>
                      <td>
                89                                  <span class="standards__ratio">x0.81</span>
                            </td>
                      <td>
                124                                  <span class="standards__ratio">x1.13</span>
                            </td>
                      <td>
                163                                  <span class="standards__ratio">x1.49</span>
                            </td>
                </tr>
          <tr>
                      <td>
                120                                  <span class="standards__ratio"></span>
                            </td>
                      <td>
                39                                  <span class="standards__ratio">x0.33</span>
                            </td>
                      <td>
                62                                  <span class="standards__ratio">x0.52</span>
                            </td>
                      <td>
                93                                  <span class="standards__ratio">x0.77</span>
                            </td>
                      <td>
                129                                  <span class="standards__ratio">x1.07</span>
                            </td>
                      <td>
                169                                  <span class="standards__ratio">x1.4</span>
                            </td>
                </tr>
          <tr>
                      <td>
                130                                  <span class="standards__ratio"></span>
                            </td>
                      <td>
                41                                  <span class="standards__ratio">x0.32</span>
                            </td>
                      <td>
                65                                  <span class="standards__ratio">x0.5</span>
                            </td>
                      <td>
                96                                  <span class="standards__ratio">x0.74</span>
                            </td>
                      <td>
                133                                  <span class="standards__ratio">x1.02</span>
                            </td>
                      <td>
                173                                  <span class="standards__ratio">x1.33</span>
                            </td>
                </tr>
          <tr>
                      <td>
                140                                  <span class="standards__ratio"></span>
                            </td>
                      <td>
                44                                  <span class="standards__ratio">x0.31</span>
                            </td>
                      <td>
                68                                  <span class="standards__ratio">x0.49</span>
                            </td>
                      <td>
                99                                  <span class="standards__ratio">x0.71</span>
                            </td>
                      <td>
                137                                  <span class="standards__ratio">x0.98</span>
                            </td>
                      <td>
                178                                  <span class="standards__ratio">x1.27</span>
                            </td>
                </tr>
          <tr>
                      <td>
                150                                  <span class="standards__ratio"></span>
                            </td>
                      <td>
                46                                  <span class="standards__ratio">x0.3</span>
                            </td>
                      <td>
                71                                  <span class="standards__ratio">x0.47</span>
                            </td>
                      <td>
                102                                  <span class="standards__ratio">x0.68</span>
                            </td>
                      <td>
                140                                  <span class="standards__ratio">x0.93</span>
                            </td>
                      <td>
                182                                  <span class="standards__ratio">x1.21</span>
                            </td>
                </tr>
          <tr>
                      <td>
                160                                  <span class="standards__ratio"></span>
                            </td>
                      <td>
                48                                  <span class="standards__ratio">x0.3</span>
                            </td>
                      <td>
                73                                  <span class="standards__ratio">x0.46</span>
                            </td>
                      <td>
                105                                  <span class="standards__ratio">x0.66</span>
                            </td>
                      <td>
                144                                  <span class="standards__ratio">x0.9</span>
                            </td>
                      <td>
                186                                  <span class="standards__ratio">x1.16</span>
                            </td>
                </tr>
          <tr>
                      <td>
                170                                  <span class="standards__ratio"></span>
                            </td>
                      <td>
                50                                  <span class="standards__ratio">x0.29</span>
                            </td>
                      <td>
                75                                  <span class="standards__ratio">x0.44</span>
                            </td>
                      <td>
                108                                  <span class="standards__ratio">x0.64</span>
                            </td>
                      <td>
                147                                  <span class="standards__ratio">x0.86</span>
                            </td>
                      <td>
                189                                  <span class="standards__ratio">x1.11</span>
                            </td>
                </tr>
          <tr>
                      <td>
                180                                  <span class="standards__ratio"></span>
                            </td>
                      <td>
                51                                  <span class="standards__ratio">x0.29</span>
                            </td>
                      <td>
                78                                  <span class="standards__ratio">x0.43</span>
                            </td>
                      <td>
                111                                  <span class="standards__ratio">x0.62</span>
                            </td>
                      <td>
                150                                  <span class="standards__ratio">x0.83</span>
                            </td>
                      <td>
                193                                  <span class="standards__ratio">x1.07</span>
                            </td>
                </tr>
          <tr>
                      <td>
                190                                  <span class="standards__ratio"></span>
                            </td>
                      <td>
                53                                  <span class="standards__ratio">x0.28</span>
                            </td>
                      <td>
                80                                  <span class="standards__ratio">x0.42</span>
                            </td>
                      <td>
                113                                  <span class="standards__ratio">x0.6</span>
                            </td>
                      <td>
                153                                  <span class="standards__ratio">x0.81</span>
                            </td>
                      <td>
                196                                  <span class="standards__ratio">x1.03</span>
                            </td>
                </tr>
          <tr>
                      <td>
                200                                  <span class="standards__ratio"></span>
                            </td>
                      <td>
                55                                  <span class="standards__ratio">x0.27</span>
                            </td>
                      <td>
                82                                  <span class="standards__ratio">x0.41</span>
                            </td>
                      <td>
                116                                  <span class="standards__ratio">x0.58</span>
                            </td>
                      <td>
                156                                  <span class="standards__ratio">x0.78</span>
                            </td>
                      <td>
                200                                  <span class="standards__ratio">x1</span>
                            </td>
                </tr>
          <tr>
                      <td>
                210                                  <span class="standards__ratio"></span>
                            </td>
                      <td>
                56                                  <span class="standards__ratio">x0.27</span>
                            </td>
                      <td>
                84                                  <span class="standards__ratio">x0.4</span>
                            </td>
                      <td>
                118                                  <span class="standards__ratio">x0.56</span>
                            </td>
                      <td>
                159                                  <span class="standards__ratio">x0.76</span>
                            </td>
                      <td>
                203                                  <span class="standards__ratio">x0.97</span>
                            </td>
                </tr>
          <tr>
                      <td>
                220                                  <span class="standards__ratio"></span>
                            </td>
                      <td>
                58                                  <span class="standards__ratio">x0.26</span>
                            </td>
                      <td>
                86                                  <span class="standards__ratio">x0.39</span>
                            </td>
                      <td>
                121                                  <span class="standards__ratio">x0.55</span>
                            </td>
                      <td>
                161                                  <span class="standards__ratio">x0.73</span>
                            </td>
                      <td>
                206                                  <span class="standards__ratio">x0.93</span>
                            </td>
                </tr>
          <tr>
                      <td>
                230                                  <span class="standards__ratio"></span>
                            </td>
                      <td>
                60                                  <span class="standards__ratio">x0.26</span>
                            </td>
                      <td>
                88                                  <span class="standards__ratio">x0.38</span>
                            </td>
                      <td>
                123                                  <span class="standards__ratio">x0.53</span>
                            </td>
                      <td>
                164                                  <span class="standards__ratio">x0.71</span>
                            </td>
                      <td>
                209                                  <span class="standards__ratio">x0.91</span>
                            </td>
                </tr>
          <tr>
                      <td>
                240                                  <span class="standards__ratio"></span>
                            </td>
                      <td>
                61                                  <span class="standards__ratio">x0.25</span>
                            </td>
                      <td>
                89                                  <span class="standards__ratio">x0.37</span>
                            </td>
                      <td>
                125                                  <span class="standards__ratio">x0.52</span>
                            </td>
                      <td>
                166                                  <span class="standards__ratio">x0.69</span>
                            </td>
                      <td>
                211                                  <span class="standards__ratio">x0.88</span>
                            </td>
                </tr>
          <tr>
                      <td>
                250                                  <span class="standards__ratio"></span>
                            </td>
                      <td>
                62                                  <span class="standards__ratio">x0.25</span>
                            </td>
                      <td>
                91                                  <span class="standards__ratio">x0.36</span>
                            </td>
                      <td>
                127                                  <span class="standards__ratio">x0.51</span>
                            </td>
                      <td>
                169                                  <span class="standards__ratio">x0.67</span>
                            </td>
                      <td>
                214                                  <span class="standards__ratio">x0.86</span>
                            </td>
                </tr>
          <tr>
                      <td>
                260                                  <span class="standards__ratio"></span>
                            </td>
                      <td>
                64                                  <span class="standards__ratio">x0.25</span>
                            </td>
                      <td>
                93                                  <span class="standards__ratio">x0.36</span>
                            </td>
                      <td>
                129                                  <span class="standards__ratio">x0.5</span>
                            </td>
                      <td>
                171                                  <span class="standards__ratio">x0.66</span>
                            </td>
                      <td>
                217                                  <span class="standards__ratio">x0.83</span>
                            </td>
                </tr>
        </tbody>
              <tfoot class="standards__footer">
        <tr>
                          <td>
                  All              </td>
                          <td>
                  43              </td>
                          <td>
                  68              </td>
                          <td>
                  100              </td>
                          <td>
                  139              </td>
                          <td>
                  182              </td>
                    </tr>
        </tfoot>
        </table>
    `,
    'deadlift':
    `
    <table class="table is-fullwidth is-striped is-narrow standards__table" id="tableId601a1cc4f276b">
    <thead>
    <tr>
      <th style="width: 25%;">
        <abbr title="Bodyweight"><span>BW</span></abbr>
      </th>
                  <th style="width: 15%;">
            <abbr
                title="Beginner"><span>Beg.</span></abbr>
          </th>
                  <th style="width: 15%;">
            <abbr
                title="Novice"><span>Nov.</span></abbr>
          </th>
                  <th style="width: 15%;">
            <abbr
                title="Intermediate"><span>Int.</span></abbr>
          </th>
                  <th style="width: 15%;">
            <abbr
                title="Advanced"><span>Adv.</span></abbr>
          </th>
                  <th style="width: 15%;">
            <abbr
                title="Elite"><span>Elite</span></abbr>
          </th>
            </tr>
    </thead>
    <tbody>
          <tr>
                      <td>
                110                                  <span class="standards__ratio"></span>
                            </td>
                      <td>
                93                                  <span class="standards__ratio">x0.85</span>
                            </td>
                      <td>
                141                                  <span class="standards__ratio">x1.28</span>
                            </td>
                      <td>
                201                                  <span class="standards__ratio">x1.82</span>
                            </td>
                      <td>
                271                                  <span class="standards__ratio">x2.47</span>
                            </td>
                      <td>
                349                                  <span class="standards__ratio">x3.17</span>
                            </td>
                </tr>
          <tr>
                      <td>
                120                                  <span class="standards__ratio"></span>
                            </td>
                      <td>
                108                                  <span class="standards__ratio">x0.9</span>
                            </td>
                      <td>
                159                                  <span class="standards__ratio">x1.33</span>
                            </td>
                      <td>
                223                                  <span class="standards__ratio">x1.85</span>
                            </td>
                      <td>
                297                                  <span class="standards__ratio">x2.47</span>
                            </td>
                      <td>
                377                                  <span class="standards__ratio">x3.14</span>
                            </td>
                </tr>
          <tr>
                      <td>
                130                                  <span class="standards__ratio"></span>
                            </td>
                      <td>
                123                                  <span class="standards__ratio">x0.95</span>
                            </td>
                      <td>
                177                                  <span class="standards__ratio">x1.36</span>
                            </td>
                      <td>
                243                                  <span class="standards__ratio">x1.87</span>
                            </td>
                      <td>
                321                                  <span class="standards__ratio">x2.47</span>
                            </td>
                      <td>
                404                                  <span class="standards__ratio">x3.11</span>
                            </td>
                </tr>
          <tr>
                      <td>
                140                                  <span class="standards__ratio"></span>
                            </td>
                      <td>
                138                                  <span class="standards__ratio">x0.98</span>
                            </td>
                      <td>
                194                                  <span class="standards__ratio">x1.39</span>
                            </td>
                      <td>
                264                                  <span class="standards__ratio">x1.88</span>
                            </td>
                      <td>
                344                                  <span class="standards__ratio">x2.46</span>
                            </td>
                      <td>
                430                                  <span class="standards__ratio">x3.07</span>
                            </td>
                </tr>
          <tr>
                      <td>
                150                                  <span class="standards__ratio"></span>
                            </td>
                      <td>
                152                                  <span class="standards__ratio">x1.01</span>
                            </td>
                      <td>
                211                                  <span class="standards__ratio">x1.41</span>
                            </td>
                      <td>
                283                                  <span class="standards__ratio">x1.89</span>
                            </td>
                      <td>
                366                                  <span class="standards__ratio">x2.44</span>
                            </td>
                      <td>
                455                                  <span class="standards__ratio">x3.04</span>
                            </td>
                </tr>
          <tr>
                      <td>
                160                                  <span class="standards__ratio"></span>
                            </td>
                      <td>
                166                                  <span class="standards__ratio">x1.04</span>
                            </td>
                      <td>
                228                                  <span class="standards__ratio">x1.42</span>
                            </td>
                      <td>
                302                                  <span class="standards__ratio">x1.89</span>
                            </td>
                      <td>
                388                                  <span class="standards__ratio">x2.42</span>
                            </td>
                      <td>
                479                                  <span class="standards__ratio">x3</span>
                            </td>
                </tr>
          <tr>
                      <td>
                170                                  <span class="standards__ratio"></span>
                            </td>
                      <td>
                180                                  <span class="standards__ratio">x1.06</span>
                            </td>
                      <td>
                244                                  <span class="standards__ratio">x1.43</span>
                            </td>
                      <td>
                321                                  <span class="standards__ratio">x1.89</span>
                            </td>
                      <td>
                409                                  <span class="standards__ratio">x2.4</span>
                            </td>
                      <td>
                502                                  <span class="standards__ratio">x2.96</span>
                            </td>
                </tr>
          <tr>
                      <td>
                180                                  <span class="standards__ratio"></span>
                            </td>
                      <td>
                193                                  <span class="standards__ratio">x1.07</span>
                            </td>
                      <td>
                259                                  <span class="standards__ratio">x1.44</span>
                            </td>
                      <td>
                339                                  <span class="standards__ratio">x1.88</span>
                            </td>
                      <td>
                429                                  <span class="standards__ratio">x2.38</span>
                            </td>
                      <td>
                525                                  <span class="standards__ratio">x2.92</span>
                            </td>
                </tr>
          <tr>
                      <td>
                190                                  <span class="standards__ratio"></span>
                            </td>
                      <td>
                206                                  <span class="standards__ratio">x1.09</span>
                            </td>
                      <td>
                274                                  <span class="standards__ratio">x1.44</span>
                            </td>
                      <td>
                356                                  <span class="standards__ratio">x1.87</span>
                            </td>
                      <td>
                448                                  <span class="standards__ratio">x2.36</span>
                            </td>
                      <td>
                546                                  <span class="standards__ratio">x2.88</span>
                            </td>
                </tr>
          <tr>
                      <td>
                200                                  <span class="standards__ratio"></span>
                            </td>
                      <td>
                219                                  <span class="standards__ratio">x1.1</span>
                            </td>
                      <td>
                289                                  <span class="standards__ratio">x1.45</span>
                            </td>
                      <td>
                373                                  <span class="standards__ratio">x1.86</span>
                            </td>
                      <td>
                467                                  <span class="standards__ratio">x2.34</span>
                            </td>
                      <td>
                567                                  <span class="standards__ratio">x2.84</span>
                            </td>
                </tr>
          <tr>
                      <td>
                210                                  <span class="standards__ratio"></span>
                            </td>
                      <td>
                232                                  <span class="standards__ratio">x1.1</span>
                            </td>
                      <td>
                303                                  <span class="standards__ratio">x1.45</span>
                            </td>
                      <td>
                389                                  <span class="standards__ratio">x1.85</span>
                            </td>
                      <td>
                485                                  <span class="standards__ratio">x2.31</span>
                            </td>
                      <td>
                587                                  <span class="standards__ratio">x2.8</span>
                            </td>
                </tr>
          <tr>
                      <td>
                220                                  <span class="standards__ratio"></span>
                            </td>
                      <td>
                244                                  <span class="standards__ratio">x1.11</span>
                            </td>
                      <td>
                318                                  <span class="standards__ratio">x1.44</span>
                            </td>
                      <td>
                405                                  <span class="standards__ratio">x1.84</span>
                            </td>
                      <td>
                503                                  <span class="standards__ratio">x2.29</span>
                            </td>
                      <td>
                607                                  <span class="standards__ratio">x2.76</span>
                            </td>
                </tr>
          <tr>
                      <td>
                230                                  <span class="standards__ratio"></span>
                            </td>
                      <td>
                256                                  <span class="standards__ratio">x1.11</span>
                            </td>
                      <td>
                331                                  <span class="standards__ratio">x1.44</span>
                            </td>
                      <td>
                421                                  <span class="standards__ratio">x1.83</span>
                            </td>
                      <td>
                520                                  <span class="standards__ratio">x2.26</span>
                            </td>
                      <td>
                626                                  <span class="standards__ratio">x2.72</span>
                            </td>
                </tr>
          <tr>
                      <td>
                240                                  <span class="standards__ratio"></span>
                            </td>
                      <td>
                268                                  <span class="standards__ratio">x1.12</span>
                            </td>
                      <td>
                345                                  <span class="standards__ratio">x1.44</span>
                            </td>
                      <td>
                436                                  <span class="standards__ratio">x1.82</span>
                            </td>
                      <td>
                537                                  <span class="standards__ratio">x2.24</span>
                            </td>
                      <td>
                644                                  <span class="standards__ratio">x2.68</span>
                            </td>
                </tr>
          <tr>
                      <td>
                250                                  <span class="standards__ratio"></span>
                            </td>
                      <td>
                280                                  <span class="standards__ratio">x1.12</span>
                            </td>
                      <td>
                358                                  <span class="standards__ratio">x1.43</span>
                            </td>
                      <td>
                451                                  <span class="standards__ratio">x1.8</span>
                            </td>
                      <td>
                554                                  <span class="standards__ratio">x2.21</span>
                            </td>
                      <td>
                662                                  <span class="standards__ratio">x2.65</span>
                            </td>
                </tr>
          <tr>
                      <td>
                260                                  <span class="standards__ratio"></span>
                            </td>
                      <td>
                291                                  <span class="standards__ratio">x1.12</span>
                            </td>
                      <td>
                371                                  <span class="standards__ratio">x1.43</span>
                            </td>
                      <td>
                465                                  <span class="standards__ratio">x1.79</span>
                            </td>
                      <td>
                570                                  <span class="standards__ratio">x2.19</span>
                            </td>
                      <td>
                680                                  <span class="standards__ratio">x2.61</span>
                            </td>
                </tr>
          <tr>
                      <td>
                270                                  <span class="standards__ratio"></span>
                            </td>
                      <td>
                302                                  <span class="standards__ratio">x1.12</span>
                            </td>
                      <td>
                383                                  <span class="standards__ratio">x1.42</span>
                            </td>
                      <td>
                479                                  <span class="standards__ratio">x1.77</span>
                            </td>
                      <td>
                585                                  <span class="standards__ratio">x2.17</span>
                            </td>
                      <td>
                697                                  <span class="standards__ratio">x2.58</span>
                            </td>
                </tr>
          <tr>
                      <td>
                280                                  <span class="standards__ratio"></span>
                            </td>
                      <td>
                313                                  <span class="standards__ratio">x1.12</span>
                            </td>
                      <td>
                396                                  <span class="standards__ratio">x1.41</span>
                            </td>
                      <td>
                493                                  <span class="standards__ratio">x1.76</span>
                            </td>
                      <td>
                600                                  <span class="standards__ratio">x2.14</span>
                            </td>
                      <td>
                713                                  <span class="standards__ratio">x2.55</span>
                            </td>
                </tr>
          <tr>
                      <td>
                290                                  <span class="standards__ratio"></span>
                            </td>
                      <td>
                324                                  <span class="standards__ratio">x1.12</span>
                            </td>
                      <td>
                408                                  <span class="standards__ratio">x1.41</span>
                            </td>
                      <td>
                506                                  <span class="standards__ratio">x1.75</span>
                            </td>
                      <td>
                615                                  <span class="standards__ratio">x2.12</span>
                            </td>
                      <td>
                729                                  <span class="standards__ratio">x2.52</span>
                            </td>
                </tr>
          <tr>
                      <td>
                300                                  <span class="standards__ratio"></span>
                            </td>
                      <td>
                334                                  <span class="standards__ratio">x1.11</span>
                            </td>
                      <td>
                420                                  <span class="standards__ratio">x1.4</span>
                            </td>
                      <td>
                519                                  <span class="standards__ratio">x1.73</span>
                            </td>
                      <td>
                630                                  <span class="standards__ratio">x2.1</span>
                            </td>
                      <td>
                745                                  <span class="standards__ratio">x2.48</span>
                            </td>
                </tr>
          <tr>
                      <td>
                310                                  <span class="standards__ratio"></span>
                            </td>
                      <td>
                345                                  <span class="standards__ratio">x1.11</span>
                            </td>
                      <td>
                431                                  <span class="standards__ratio">x1.39</span>
                            </td>
                      <td>
                532                                  <span class="standards__ratio">x1.72</span>
                            </td>
                      <td>
                644                                  <span class="standards__ratio">x2.08</span>
                            </td>
                      <td>
                761                                  <span class="standards__ratio">x2.45</span>
                            </td>
                </tr>
        </tbody>
              <tfoot class="standards__footer">
        <tr>
                          <td>
                  All              </td>
                          <td>
                  171              </td>
                          <td>
                  244              </td>
                          <td>
                  334              </td>
                          <td>
                  439              </td>
                          <td>
                  552              </td>
                    </tr>
        </tfoot>
        </table>
        <table class="table is-fullwidth is-striped is-narrow standards__table" id="tableId601a1cc4f287a">
    <thead>
    <tr>
      <th style="width: 25%;">
        <abbr title="Bodyweight"><span>BW</span></abbr>
      </th>
                  <th style="width: 15%;">
            <abbr
                title="Beginner"><span>Beg.</span></abbr>
          </th>
                  <th style="width: 15%;">
            <abbr
                title="Novice"><span>Nov.</span></abbr>
          </th>
                  <th style="width: 15%;">
            <abbr
                title="Intermediate"><span>Int.</span></abbr>
          </th>
                  <th style="width: 15%;">
            <abbr
                title="Advanced"><span>Adv.</span></abbr>
          </th>
                  <th style="width: 15%;">
            <abbr
                title="Elite"><span>Elite</span></abbr>
          </th>
            </tr>
    </thead>
    <tbody>
          <tr>
                      <td>
                90                                  <span class="standards__ratio"></span>
                            </td>
                      <td>
                54                                  <span class="standards__ratio">x0.6</span>
                            </td>
                      <td>
                91                                  <span class="standards__ratio">x1.01</span>
                            </td>
                      <td>
                139                                  <span class="standards__ratio">x1.55</span>
                            </td>
                      <td>
                199                                  <span class="standards__ratio">x2.21</span>
                            </td>
                      <td>
                265                                  <span class="standards__ratio">x2.94</span>
                            </td>
                </tr>
          <tr>
                      <td>
                100                                  <span class="standards__ratio"></span>
                            </td>
                      <td>
                61                                  <span class="standards__ratio">x0.61</span>
                            </td>
                      <td>
                100                                  <span class="standards__ratio">x1</span>
                            </td>
                      <td>
                151                                  <span class="standards__ratio">x1.51</span>
                            </td>
                      <td>
                212                                  <span class="standards__ratio">x2.12</span>
                            </td>
                      <td>
                280                                  <span class="standards__ratio">x2.8</span>
                            </td>
                </tr>
          <tr>
                      <td>
                110                                  <span class="standards__ratio"></span>
                            </td>
                      <td>
                68                                  <span class="standards__ratio">x0.62</span>
                            </td>
                      <td>
                109                                  <span class="standards__ratio">x0.99</span>
                            </td>
                      <td>
                161                                  <span class="standards__ratio">x1.47</span>
                            </td>
                      <td>
                225                                  <span class="standards__ratio">x2.04</span>
                            </td>
                      <td>
                295                                  <span class="standards__ratio">x2.68</span>
                            </td>
                </tr>
          <tr>
                      <td>
                120                                  <span class="standards__ratio"></span>
                            </td>
                      <td>
                74                                  <span class="standards__ratio">x0.62</span>
                            </td>
                      <td>
                117                                  <span class="standards__ratio">x0.97</span>
                            </td>
                      <td>
                171                                  <span class="standards__ratio">x1.43</span>
                            </td>
                      <td>
                237                                  <span class="standards__ratio">x1.97</span>
                            </td>
                      <td>
                308                                  <span class="standards__ratio">x2.57</span>
                            </td>
                </tr>
          <tr>
                      <td>
                130                                  <span class="standards__ratio"></span>
                            </td>
                      <td>
                81                                  <span class="standards__ratio">x0.62</span>
                            </td>
                      <td>
                125                                  <span class="standards__ratio">x0.96</span>
                            </td>
                      <td>
                181                                  <span class="standards__ratio">x1.39</span>
                            </td>
                      <td>
                248                                  <span class="standards__ratio">x1.91</span>
                            </td>
                      <td>
                321                                  <span class="standards__ratio">x2.47</span>
                            </td>
                </tr>
          <tr>
                      <td>
                140                                  <span class="standards__ratio"></span>
                            </td>
                      <td>
                87                                  <span class="standards__ratio">x0.62</span>
                            </td>
                      <td>
                132                                  <span class="standards__ratio">x0.94</span>
                            </td>
                      <td>
                190                                  <span class="standards__ratio">x1.36</span>
                            </td>
                      <td>
                258                                  <span class="standards__ratio">x1.84</span>
                            </td>
                      <td>
                333                                  <span class="standards__ratio">x2.38</span>
                            </td>
                </tr>
          <tr>
                      <td>
                150                                  <span class="standards__ratio"></span>
                            </td>
                      <td>
                93                                  <span class="standards__ratio">x0.62</span>
                            </td>
                      <td>
                139                                  <span class="standards__ratio">x0.93</span>
                            </td>
                      <td>
                199                                  <span class="standards__ratio">x1.32</span>
                            </td>
                      <td>
                268                                  <span class="standards__ratio">x1.79</span>
                            </td>
                      <td>
                344                                  <span class="standards__ratio">x2.3</span>
                            </td>
                </tr>
          <tr>
                      <td>
                160                                  <span class="standards__ratio"></span>
                            </td>
                      <td>
                98                                  <span class="standards__ratio">x0.62</span>
                            </td>
                      <td>
                146                                  <span class="standards__ratio">x0.92</span>
                            </td>
                      <td>
                207                                  <span class="standards__ratio">x1.29</span>
                            </td>
                      <td>
                278                                  <span class="standards__ratio">x1.74</span>
                            </td>
                      <td>
                355                                  <span class="standards__ratio">x2.22</span>
                            </td>
                </tr>
          <tr>
                      <td>
                170                                  <span class="standards__ratio"></span>
                            </td>
                      <td>
                104                                  <span class="standards__ratio">x0.61</span>
                            </td>
                      <td>
                153                                  <span class="standards__ratio">x0.9</span>
                            </td>
                      <td>
                215                                  <span class="standards__ratio">x1.26</span>
                            </td>
                      <td>
                287                                  <span class="standards__ratio">x1.69</span>
                            </td>
                      <td>
                366                                  <span class="standards__ratio">x2.15</span>
                            </td>
                </tr>
          <tr>
                      <td>
                180                                  <span class="standards__ratio"></span>
                            </td>
                      <td>
                109                                  <span class="standards__ratio">x0.61</span>
                            </td>
                      <td>
                159                                  <span class="standards__ratio">x0.89</span>
                            </td>
                      <td>
                222                                  <span class="standards__ratio">x1.24</span>
                            </td>
                      <td>
                296                                  <span class="standards__ratio">x1.64</span>
                            </td>
                      <td>
                375                                  <span class="standards__ratio">x2.09</span>
                            </td>
                </tr>
          <tr>
                      <td>
                190                                  <span class="standards__ratio"></span>
                            </td>
                      <td>
                114                                  <span class="standards__ratio">x0.6</span>
                            </td>
                      <td>
                166                                  <span class="standards__ratio">x0.87</span>
                            </td>
                      <td>
                230                                  <span class="standards__ratio">x1.21</span>
                            </td>
                      <td>
                304                                  <span class="standards__ratio">x1.6</span>
                            </td>
                      <td>
                385                                  <span class="standards__ratio">x2.03</span>
                            </td>
                </tr>
          <tr>
                      <td>
                200                                  <span class="standards__ratio"></span>
                            </td>
                      <td>
                119                                  <span class="standards__ratio">x0.6</span>
                            </td>
                      <td>
                172                                  <span class="standards__ratio">x0.86</span>
                            </td>
                      <td>
                237                                  <span class="standards__ratio">x1.18</span>
                            </td>
                      <td>
                312                                  <span class="standards__ratio">x1.56</span>
                            </td>
                      <td>
                394                                  <span class="standards__ratio">x1.97</span>
                            </td>
                </tr>
          <tr>
                      <td>
                210                                  <span class="standards__ratio"></span>
                            </td>
                      <td>
                124                                  <span class="standards__ratio">x0.59</span>
                            </td>
                      <td>
                177                                  <span class="standards__ratio">x0.84</span>
                            </td>
                      <td>
                243                                  <span class="standards__ratio">x1.16</span>
                            </td>
                      <td>
                320                                  <span class="standards__ratio">x1.52</span>
                            </td>
                      <td>
                403                                  <span class="standards__ratio">x1.92</span>
                            </td>
                </tr>
          <tr>
                      <td>
                220                                  <span class="standards__ratio"></span>
                            </td>
                      <td>
                129                                  <span class="standards__ratio">x0.59</span>
                            </td>
                      <td>
                183                                  <span class="standards__ratio">x0.83</span>
                            </td>
                      <td>
                250                                  <span class="standards__ratio">x1.14</span>
                            </td>
                      <td>
                327                                  <span class="standards__ratio">x1.49</span>
                            </td>
                      <td>
                411                                  <span class="standards__ratio">x1.87</span>
                            </td>
                </tr>
          <tr>
                      <td>
                230                                  <span class="standards__ratio"></span>
                            </td>
                      <td>
                133                                  <span class="standards__ratio">x0.58</span>
                            </td>
                      <td>
                188                                  <span class="standards__ratio">x0.82</span>
                            </td>
                      <td>
                256                                  <span class="standards__ratio">x1.11</span>
                            </td>
                      <td>
                335                                  <span class="standards__ratio">x1.46</span>
                            </td>
                      <td>
                419                                  <span class="standards__ratio">x1.82</span>
                            </td>
                </tr>
          <tr>
                      <td>
                240                                  <span class="standards__ratio"></span>
                            </td>
                      <td>
                138                                  <span class="standards__ratio">x0.57</span>
                            </td>
                      <td>
                194                                  <span class="standards__ratio">x0.81</span>
                            </td>
                      <td>
                262                                  <span class="standards__ratio">x1.09</span>
                            </td>
                      <td>
                342                                  <span class="standards__ratio">x1.42</span>
                            </td>
                      <td>
                427                                  <span class="standards__ratio">x1.78</span>
                            </td>
                </tr>
          <tr>
                      <td>
                250                                  <span class="standards__ratio"></span>
                            </td>
                      <td>
                142                                  <span class="standards__ratio">x0.57</span>
                            </td>
                      <td>
                199                                  <span class="standards__ratio">x0.8</span>
                            </td>
                      <td>
                268                                  <span class="standards__ratio">x1.07</span>
                            </td>
                      <td>
                348                                  <span class="standards__ratio">x1.39</span>
                            </td>
                      <td>
                435                                  <span class="standards__ratio">x1.74</span>
                            </td>
                </tr>
          <tr>
                      <td>
                260                                  <span class="standards__ratio"></span>
                            </td>
                      <td>
                146                                  <span class="standards__ratio">x0.56</span>
                            </td>
                      <td>
                204                                  <span class="standards__ratio">x0.78</span>
                            </td>
                      <td>
                274                                  <span class="standards__ratio">x1.05</span>
                            </td>
                      <td>
                355                                  <span class="standards__ratio">x1.37</span>
                            </td>
                      <td>
                442                                  <span class="standards__ratio">x1.7</span>
                            </td>
                </tr>
        </tbody>
              <tfoot class="standards__footer">
        <tr>
                          <td>
                  All              </td>
                          <td>
                  85              </td>
                          <td>
                  132              </td>
                          <td>
                  194              </td>
                          <td>
                  267              </td>
                          <td>
                  347              </td>
                    </tr>
        </tfoot>
        </table>    
    `,
    'squat':
    `
    <table class="table is-fullwidth is-striped is-narrow standards__table" id="tableId601a1d5e11213">
    <thead>
    <tr>
      <th style="width: 25%;">
        <abbr title="Bodyweight"><span>BW</span></abbr>
      </th>
                  <th style="width: 15%;">
            <abbr
                title="Beginner"><span>Beg.</span></abbr>
          </th>
                  <th style="width: 15%;">
            <abbr
                title="Novice"><span>Nov.</span></abbr>
          </th>
                  <th style="width: 15%;">
            <abbr
                title="Intermediate"><span>Int.</span></abbr>
          </th>
                  <th style="width: 15%;">
            <abbr
                title="Advanced"><span>Adv.</span></abbr>
          </th>
                  <th style="width: 15%;">
            <abbr
                title="Elite"><span>Elite</span></abbr>
          </th>
            </tr>
    </thead>
    <tbody>
          <tr>
                      <td>
                110                                  <span class="standards__ratio"></span>
                            </td>
                      <td>
                71                                  <span class="standards__ratio">x0.65</span>
                            </td>
                      <td>
                112                                  <span class="standards__ratio">x1.02</span>
                            </td>
                      <td>
                164                                  <span class="standards__ratio">x1.49</span>
                            </td>
                      <td>
                226                                  <span class="standards__ratio">x2.06</span>
                            </td>
                      <td>
                295                                  <span class="standards__ratio">x2.68</span>
                            </td>
                </tr>
          <tr>
                      <td>
                120                                  <span class="standards__ratio"></span>
                            </td>
                      <td>
                84                                  <span class="standards__ratio">x0.7</span>
                            </td>
                      <td>
                128                                  <span class="standards__ratio">x1.07</span>
                            </td>
                      <td>
                184                                  <span class="standards__ratio">x1.53</span>
                            </td>
                      <td>
                249                                  <span class="standards__ratio">x2.08</span>
                            </td>
                      <td>
                321                                  <span class="standards__ratio">x2.68</span>
                            </td>
                </tr>
          <tr>
                      <td>
                130                                  <span class="standards__ratio"></span>
                            </td>
                      <td>
                98                                  <span class="standards__ratio">x0.75</span>
                            </td>
                      <td>
                144                                  <span class="standards__ratio">x1.11</span>
                            </td>
                      <td>
                203                                  <span class="standards__ratio">x1.56</span>
                            </td>
                      <td>
                271                                  <span class="standards__ratio">x2.09</span>
                            </td>
                      <td>
                346                                  <span class="standards__ratio">x2.66</span>
                            </td>
                </tr>
          <tr>
                      <td>
                140                                  <span class="standards__ratio"></span>
                            </td>
                      <td>
                110                                  <span class="standards__ratio">x0.79</span>
                            </td>
                      <td>
                160                                  <span class="standards__ratio">x1.14</span>
                            </td>
                      <td>
                221                                  <span class="standards__ratio">x1.58</span>
                            </td>
                      <td>
                293                                  <span class="standards__ratio">x2.09</span>
                            </td>
                      <td>
                370                                  <span class="standards__ratio">x2.64</span>
                            </td>
                </tr>
          <tr>
                      <td>
                150                                  <span class="standards__ratio"></span>
                            </td>
                      <td>
                123                                  <span class="standards__ratio">x0.82</span>
                            </td>
                      <td>
                175                                  <span class="standards__ratio">x1.17</span>
                            </td>
                      <td>
                239                                  <span class="standards__ratio">x1.59</span>
                            </td>
                      <td>
                313                                  <span class="standards__ratio">x2.09</span>
                            </td>
                      <td>
                393                                  <span class="standards__ratio">x2.62</span>
                            </td>
                </tr>
          <tr>
                      <td>
                160                                  <span class="standards__ratio"></span>
                            </td>
                      <td>
                136                                  <span class="standards__ratio">x0.85</span>
                            </td>
                      <td>
                190                                  <span class="standards__ratio">x1.19</span>
                            </td>
                      <td>
                256                                  <span class="standards__ratio">x1.6</span>
                            </td>
                      <td>
                333                                  <span class="standards__ratio">x2.08</span>
                            </td>
                      <td>
                415                                  <span class="standards__ratio">x2.6</span>
                            </td>
                </tr>
          <tr>
                      <td>
                170                                  <span class="standards__ratio"></span>
                            </td>
                      <td>
                148                                  <span class="standards__ratio">x0.87</span>
                            </td>
                      <td>
                204                                  <span class="standards__ratio">x1.2</span>
                            </td>
                      <td>
                273                                  <span class="standards__ratio">x1.61</span>
                            </td>
                      <td>
                352                                  <span class="standards__ratio">x2.07</span>
                            </td>
                      <td>
                437                                  <span class="standards__ratio">x2.57</span>
                            </td>
                </tr>
          <tr>
                      <td>
                180                                  <span class="standards__ratio"></span>
                            </td>
                      <td>
                160                                  <span class="standards__ratio">x0.89</span>
                            </td>
                      <td>
                219                                  <span class="standards__ratio">x1.21</span>
                            </td>
                      <td>
                290                                  <span class="standards__ratio">x1.61</span>
                            </td>
                      <td>
                371                                  <span class="standards__ratio">x2.06</span>
                            </td>
                      <td>
                457                                  <span class="standards__ratio">x2.54</span>
                            </td>
                </tr>
          <tr>
                      <td>
                190                                  <span class="standards__ratio"></span>
                            </td>
                      <td>
                172                                  <span class="standards__ratio">x0.91</span>
                            </td>
                      <td>
                233                                  <span class="standards__ratio">x1.22</span>
                            </td>
                      <td>
                306                                  <span class="standards__ratio">x1.61</span>
                            </td>
                      <td>
                389                                  <span class="standards__ratio">x2.05</span>
                            </td>
                      <td>
                477                                  <span class="standards__ratio">x2.51</span>
                            </td>
                </tr>
          <tr>
                      <td>
                200                                  <span class="standards__ratio"></span>
                            </td>
                      <td>
                184                                  <span class="standards__ratio">x0.92</span>
                            </td>
                      <td>
                246                                  <span class="standards__ratio">x1.23</span>
                            </td>
                      <td>
                321                                  <span class="standards__ratio">x1.61</span>
                            </td>
                      <td>
                406                                  <span class="standards__ratio">x2.03</span>
                            </td>
                      <td>
                497                                  <span class="standards__ratio">x2.48</span>
                            </td>
                </tr>
          <tr>
                      <td>
                210                                  <span class="standards__ratio"></span>
                            </td>
                      <td>
                195                                  <span class="standards__ratio">x0.93</span>
                            </td>
                      <td>
                259                                  <span class="standards__ratio">x1.24</span>
                            </td>
                      <td>
                336                                  <span class="standards__ratio">x1.6</span>
                            </td>
                      <td>
                423                                  <span class="standards__ratio">x2.02</span>
                            </td>
                      <td>
                515                                  <span class="standards__ratio">x2.45</span>
                            </td>
                </tr>
          <tr>
                      <td>
                220                                  <span class="standards__ratio"></span>
                            </td>
                      <td>
                207                                  <span class="standards__ratio">x0.94</span>
                            </td>
                      <td>
                272                                  <span class="standards__ratio">x1.24</span>
                            </td>
                      <td>
                351                                  <span class="standards__ratio">x1.6</span>
                            </td>
                      <td>
                440                                  <span class="standards__ratio">x2</span>
                            </td>
                      <td>
                534                                  <span class="standards__ratio">x2.43</span>
                            </td>
                </tr>
          <tr>
                      <td>
                230                                  <span class="standards__ratio"></span>
                            </td>
                      <td>
                218                                  <span class="standards__ratio">x0.95</span>
                            </td>
                      <td>
                285                                  <span class="standards__ratio">x1.24</span>
                            </td>
                      <td>
                365                                  <span class="standards__ratio">x1.59</span>
                            </td>
                      <td>
                456                                  <span class="standards__ratio">x1.98</span>
                            </td>
                      <td>
                551                                  <span class="standards__ratio">x2.4</span>
                            </td>
                </tr>
          <tr>
                      <td>
                240                                  <span class="standards__ratio"></span>
                            </td>
                      <td>
                229                                  <span class="standards__ratio">x0.95</span>
                            </td>
                      <td>
                297                                  <span class="standards__ratio">x1.24</span>
                            </td>
                      <td>
                379                                  <span class="standards__ratio">x1.58</span>
                            </td>
                      <td>
                471                                  <span class="standards__ratio">x1.96</span>
                            </td>
                      <td>
                569                                  <span class="standards__ratio">x2.37</span>
                            </td>
                </tr>
          <tr>
                      <td>
                250                                  <span class="standards__ratio"></span>
                            </td>
                      <td>
                239                                  <span class="standards__ratio">x0.96</span>
                            </td>
                      <td>
                310                                  <span class="standards__ratio">x1.24</span>
                            </td>
                      <td>
                393                                  <span class="standards__ratio">x1.57</span>
                            </td>
                      <td>
                487                                  <span class="standards__ratio">x1.95</span>
                            </td>
                      <td>
                585                                  <span class="standards__ratio">x2.34</span>
                            </td>
                </tr>
          <tr>
                      <td>
                260                                  <span class="standards__ratio"></span>
                            </td>
                      <td>
                250                                  <span class="standards__ratio">x0.96</span>
                            </td>
                      <td>
                322                                  <span class="standards__ratio">x1.24</span>
                            </td>
                      <td>
                407                                  <span class="standards__ratio">x1.56</span>
                            </td>
                      <td>
                502                                  <span class="standards__ratio">x1.93</span>
                            </td>
                      <td>
                602                                  <span class="standards__ratio">x2.31</span>
                            </td>
                </tr>
          <tr>
                      <td>
                270                                  <span class="standards__ratio"></span>
                            </td>
                      <td>
                260                                  <span class="standards__ratio">x0.96</span>
                            </td>
                      <td>
                333                                  <span class="standards__ratio">x1.23</span>
                            </td>
                      <td>
                420                                  <span class="standards__ratio">x1.55</span>
                            </td>
                      <td>
                516                                  <span class="standards__ratio">x1.91</span>
                            </td>
                      <td>
                618                                  <span class="standards__ratio">x2.29</span>
                            </td>
                </tr>
          <tr>
                      <td>
                280                                  <span class="standards__ratio"></span>
                            </td>
                      <td>
                270                                  <span class="standards__ratio">x0.96</span>
                            </td>
                      <td>
                345                                  <span class="standards__ratio">x1.23</span>
                            </td>
                      <td>
                433                                  <span class="standards__ratio">x1.54</span>
                            </td>
                      <td>
                530                                  <span class="standards__ratio">x1.89</span>
                            </td>
                      <td>
                633                                  <span class="standards__ratio">x2.26</span>
                            </td>
                </tr>
          <tr>
                      <td>
                290                                  <span class="standards__ratio"></span>
                            </td>
                      <td>
                280                                  <span class="standards__ratio">x0.97</span>
                            </td>
                      <td>
                356                                  <span class="standards__ratio">x1.23</span>
                            </td>
                      <td>
                445                                  <span class="standards__ratio">x1.53</span>
                            </td>
                      <td>
                544                                  <span class="standards__ratio">x1.88</span>
                            </td>
                      <td>
                648                                  <span class="standards__ratio">x2.24</span>
                            </td>
                </tr>
          <tr>
                      <td>
                300                                  <span class="standards__ratio"></span>
                            </td>
                      <td>
                290                                  <span class="standards__ratio">x0.97</span>
                            </td>
                      <td>
                367                                  <span class="standards__ratio">x1.22</span>
                            </td>
                      <td>
                457                                  <span class="standards__ratio">x1.52</span>
                            </td>
                      <td>
                558                                  <span class="standards__ratio">x1.86</span>
                            </td>
                      <td>
                663                                  <span class="standards__ratio">x2.21</span>
                            </td>
                </tr>
          <tr>
                      <td>
                310                                  <span class="standards__ratio"></span>
                            </td>
                      <td>
                299                                  <span class="standards__ratio">x0.97</span>
                            </td>
                      <td>
                378                                  <span class="standards__ratio">x1.22</span>
                            </td>
                      <td>
                470                                  <span class="standards__ratio">x1.51</span>
                            </td>
                      <td>
                571                                  <span class="standards__ratio">x1.84</span>
                            </td>
                      <td>
                678                                  <span class="standards__ratio">x2.19</span>
                            </td>
                </tr>
        </tbody>
              <tfoot class="standards__footer">
        <tr>
                          <td>
                  All              </td>
                          <td>
                  139              </td>
                          <td>
                  204              </td>
                          <td>
                  284              </td>
                          <td>
                  379              </td>
                          <td>
                  481              </td>
                    </tr>
        </tfoot>
        </table>
        <table class="table is-fullwidth is-striped is-narrow standards__table" id="tableId601a1d5e1130f">
        <thead>
        <tr>
          <th style="width: 25%;">
            <abbr title="Bodyweight"><span>BW</span></abbr>
          </th>
                      <th style="width: 15%;">
                <abbr
                    title="Beginner"><span>Beg.</span></abbr>
              </th>
                      <th style="width: 15%;">
                <abbr
                    title="Novice"><span>Nov.</span></abbr>
              </th>
                      <th style="width: 15%;">
                <abbr
                    title="Intermediate"><span>Int.</span></abbr>
              </th>
                      <th style="width: 15%;">
                <abbr
                    title="Advanced"><span>Adv.</span></abbr>
              </th>
                      <th style="width: 15%;">
                <abbr
                    title="Elite"><span>Elite</span></abbr>
              </th>
                </tr>
        </thead>
        <tbody>
              <tr>
                          <td>
                    90                                  <span class="standards__ratio"></span>
                                </td>
                          <td>
                    40                                  <span class="standards__ratio">x0.44</span>
                                </td>
                          <td>
                    71                                  <span class="standards__ratio">x0.79</span>
                                </td>
                          <td>
                    114                                  <span class="standards__ratio">x1.27</span>
                                </td>
                          <td>
                    167                                  <span class="standards__ratio">x1.85</span>
                                </td>
                          <td>
                    226                                  <span class="standards__ratio">x2.51</span>
                                </td>
                    </tr>
              <tr>
                          <td>
                    100                                  <span class="standards__ratio"></span>
                                </td>
                          <td>
                    46                                  <span class="standards__ratio">x0.46</span>
                                </td>
                          <td>
                    79                                  <span class="standards__ratio">x0.79</span>
                                </td>
                          <td>
                    124                                  <span class="standards__ratio">x1.24</span>
                                </td>
                          <td>
                    179                                  <span class="standards__ratio">x1.79</span>
                                </td>
                          <td>
                    241                                  <span class="standards__ratio">x2.41</span>
                                </td>
                    </tr>
              <tr>
                          <td>
                    110                                  <span class="standards__ratio"></span>
                                </td>
                          <td>
                    52                                  <span class="standards__ratio">x0.47</span>
                                </td>
                          <td>
                    87                                  <span class="standards__ratio">x0.79</span>
                                </td>
                          <td>
                    134                                  <span class="standards__ratio">x1.22</span>
                                </td>
                          <td>
                    191                                  <span class="standards__ratio">x1.73</span>
                                </td>
                          <td>
                    254                                  <span class="standards__ratio">x2.31</span>
                                </td>
                    </tr>
              <tr>
                          <td>
                    120                                  <span class="standards__ratio"></span>
                                </td>
                          <td>
                    58                                  <span class="standards__ratio">x0.48</span>
                                </td>
                          <td>
                    95                                  <span class="standards__ratio">x0.79</span>
                                </td>
                          <td>
                    143                                  <span class="standards__ratio">x1.19</span>
                                </td>
                          <td>
                    202                                  <span class="standards__ratio">x1.68</span>
                                </td>
                          <td>
                    267                                  <span class="standards__ratio">x2.22</span>
                                </td>
                    </tr>
              <tr>
                          <td>
                    130                                  <span class="standards__ratio"></span>
                                </td>
                          <td>
                    63                                  <span class="standards__ratio">x0.49</span>
                                </td>
                          <td>
                    102                                  <span class="standards__ratio">x0.78</span>
                                </td>
                          <td>
                    152                                  <span class="standards__ratio">x1.17</span>
                                </td>
                          <td>
                    212                                  <span class="standards__ratio">x1.63</span>
                                </td>
                          <td>
                    278                                  <span class="standards__ratio">x2.14</span>
                                </td>
                    </tr>
              <tr>
                          <td>
                    140                                  <span class="standards__ratio"></span>
                                </td>
                          <td>
                    69                                  <span class="standards__ratio">x0.49</span>
                                </td>
                          <td>
                    109                                  <span class="standards__ratio">x0.78</span>
                                </td>
                          <td>
                    160                                  <span class="standards__ratio">x1.14</span>
                                </td>
                          <td>
                    222                                  <span class="standards__ratio">x1.58</span>
                                </td>
                          <td>
                    290                                  <span class="standards__ratio">x2.07</span>
                                </td>
                    </tr>
              <tr>
                          <td>
                    150                                  <span class="standards__ratio"></span>
                                </td>
                          <td>
                    74                                  <span class="standards__ratio">x0.49</span>
                                </td>
                          <td>
                    115                                  <span class="standards__ratio">x0.77</span>
                                </td>
                          <td>
                    168                                  <span class="standards__ratio">x1.12</span>
                                </td>
                          <td>
                    231                                  <span class="standards__ratio">x1.54</span>
                                </td>
                          <td>
                    300                                  <span class="standards__ratio">x2</span>
                                </td>
                    </tr>
              <tr>
                          <td>
                    160                                  <span class="standards__ratio"></span>
                                </td>
                          <td>
                    79                                  <span class="standards__ratio">x0.49</span>
                                </td>
                          <td>
                    121                                  <span class="standards__ratio">x0.76</span>
                                </td>
                          <td>
                    175                                  <span class="standards__ratio">x1.1</span>
                                </td>
                          <td>
                    240                                  <span class="standards__ratio">x1.5</span>
                                </td>
                          <td>
                    310                                  <span class="standards__ratio">x1.94</span>
                                </td>
                    </tr>
              <tr>
                          <td>
                    170                                  <span class="standards__ratio"></span>
                                </td>
                          <td>
                    84                                  <span class="standards__ratio">x0.49</span>
                                </td>
                          <td>
                    127                                  <span class="standards__ratio">x0.75</span>
                                </td>
                          <td>
                    183                                  <span class="standards__ratio">x1.07</span>
                                </td>
                          <td>
                    248                                  <span class="standards__ratio">x1.46</span>
                                </td>
                          <td>
                    320                                  <span class="standards__ratio">x1.88</span>
                                </td>
                    </tr>
              <tr>
                          <td>
                    180                                  <span class="standards__ratio"></span>
                                </td>
                          <td>
                    89                                  <span class="standards__ratio">x0.49</span>
                                </td>
                          <td>
                    133                                  <span class="standards__ratio">x0.74</span>
                                </td>
                          <td>
                    190                                  <span class="standards__ratio">x1.05</span>
                                </td>
                          <td>
                    256                                  <span class="standards__ratio">x1.42</span>
                                </td>
                          <td>
                    329                                  <span class="standards__ratio">x1.83</span>
                                </td>
                    </tr>
              <tr>
                          <td>
                    190                                  <span class="standards__ratio"></span>
                                </td>
                          <td>
                    93                                  <span class="standards__ratio">x0.49</span>
                                </td>
                          <td>
                    139                                  <span class="standards__ratio">x0.73</span>
                                </td>
                          <td>
                    196                                  <span class="standards__ratio">x1.03</span>
                                </td>
                          <td>
                    264                                  <span class="standards__ratio">x1.39</span>
                                </td>
                          <td>
                    338                                  <span class="standards__ratio">x1.78</span>
                                </td>
                    </tr>
              <tr>
                          <td>
                    200                                  <span class="standards__ratio"></span>
                                </td>
                          <td>
                    98                                  <span class="standards__ratio">x0.49</span>
                                </td>
                          <td>
                    144                                  <span class="standards__ratio">x0.72</span>
                                </td>
                          <td>
                    203                                  <span class="standards__ratio">x1.01</span>
                                </td>
                          <td>
                    271                                  <span class="standards__ratio">x1.36</span>
                                </td>
                          <td>
                    346                                  <span class="standards__ratio">x1.73</span>
                                </td>
                    </tr>
              <tr>
                          <td>
                    210                                  <span class="standards__ratio"></span>
                                </td>
                          <td>
                    102                                  <span class="standards__ratio">x0.49</span>
                                </td>
                          <td>
                    150                                  <span class="standards__ratio">x0.71</span>
                                </td>
                          <td>
                    209                                  <span class="standards__ratio">x1</span>
                                </td>
                          <td>
                    279                                  <span class="standards__ratio">x1.33</span>
                                </td>
                          <td>
                    354                                  <span class="standards__ratio">x1.69</span>
                                </td>
                    </tr>
              <tr>
                          <td>
                    220                                  <span class="standards__ratio"></span>
                                </td>
                          <td>
                    106                                  <span class="standards__ratio">x0.48</span>
                                </td>
                          <td>
                    155                                  <span class="standards__ratio">x0.7</span>
                                </td>
                          <td>
                    215                                  <span class="standards__ratio">x0.98</span>
                                </td>
                          <td>
                    286                                  <span class="standards__ratio">x1.3</span>
                                </td>
                          <td>
                    362                                  <span class="standards__ratio">x1.65</span>
                                </td>
                    </tr>
              <tr>
                          <td>
                    230                                  <span class="standards__ratio"></span>
                                </td>
                          <td>
                    110                                  <span class="standards__ratio">x0.48</span>
                                </td>
                          <td>
                    160                                  <span class="standards__ratio">x0.69</span>
                                </td>
                          <td>
                    221                                  <span class="standards__ratio">x0.96</span>
                                </td>
                          <td>
                    292                                  <span class="standards__ratio">x1.27</span>
                                </td>
                          <td>
                    370                                  <span class="standards__ratio">x1.61</span>
                                </td>
                    </tr>
              <tr>
                          <td>
                    240                                  <span class="standards__ratio"></span>
                                </td>
                          <td>
                    114                                  <span class="standards__ratio">x0.48</span>
                                </td>
                          <td>
                    164                                  <span class="standards__ratio">x0.69</span>
                                </td>
                          <td>
                    227                                  <span class="standards__ratio">x0.94</span>
                                </td>
                          <td>
                    299                                  <span class="standards__ratio">x1.25</span>
                                </td>
                          <td>
                    377                                  <span class="standards__ratio">x1.57</span>
                                </td>
                    </tr>
              <tr>
                          <td>
                    250                                  <span class="standards__ratio"></span>
                                </td>
                          <td>
                    118                                  <span class="standards__ratio">x0.47</span>
                                </td>
                          <td>
                    169                                  <span class="standards__ratio">x0.68</span>
                                </td>
                          <td>
                    232                                  <span class="standards__ratio">x0.93</span>
                                </td>
                          <td>
                    305                                  <span class="standards__ratio">x1.22</span>
                                </td>
                          <td>
                    384                                  <span class="standards__ratio">x1.54</span>
                                </td>
                    </tr>
              <tr>
                          <td>
                    260                                  <span class="standards__ratio"></span>
                                </td>
                          <td>
                    122                                  <span class="standards__ratio">x0.47</span>
                                </td>
                          <td>
                    174                                  <span class="standards__ratio">x0.67</span>
                                </td>
                          <td>
                    238                                  <span class="standards__ratio">x0.91</span>
                                </td>
                          <td>
                    311                                  <span class="standards__ratio">x1.2</span>
                                </td>
                          <td>
                    391                                  <span class="standards__ratio">x1.5</span>
                                </td>
                    </tr>
            </tbody>
                  <tfoot class="standards__footer">
            <tr>
                              <td>
                      All              </td>
                              <td>
                      66              </td>
                              <td>
                      107              </td>
                              <td>
                      161              </td>
                              <td>
                      227              </td>
                              <td>
                      299              </td>
                        </tr>
            </tfoot>
            </table>            
    `,
    'shoulder-press':
    `
    <table class="table is-fullwidth is-striped is-narrow standards__table" id="tableId601a1d965af97">
    <thead>
    <tr>
      <th style="width: 25%;">
        <abbr title="Bodyweight"><span>BW</span></abbr>
      </th>
                  <th style="width: 15%;">
            <abbr
                title="Beginner"><span>Beg.</span></abbr>
          </th>
                  <th style="width: 15%;">
            <abbr
                title="Novice"><span>Nov.</span></abbr>
          </th>
                  <th style="width: 15%;">
            <abbr
                title="Intermediate"><span>Int.</span></abbr>
          </th>
                  <th style="width: 15%;">
            <abbr
                title="Advanced"><span>Adv.</span></abbr>
          </th>
                  <th style="width: 15%;">
            <abbr
                title="Elite"><span>Elite</span></abbr>
          </th>
            </tr>
    </thead>
    <tbody>
          <tr>
                      <td>
                110                                  <span class="standards__ratio"></span>
                            </td>
                      <td>
                32                                  <span class="standards__ratio">x0.29</span>
                            </td>
                      <td>
                53                                  <span class="standards__ratio">x0.48</span>
                            </td>
                      <td>
                80                                  <span class="standards__ratio">x0.73</span>
                            </td>
                      <td>
                113                                  <span class="standards__ratio">x1.03</span>
                            </td>
                      <td>
                150                                  <span class="standards__ratio">x1.37</span>
                            </td>
                </tr>
          <tr>
                      <td>
                120                                  <span class="standards__ratio"></span>
                            </td>
                      <td>
                38                                  <span class="standards__ratio">x0.32</span>
                            </td>
                      <td>
                60                                  <span class="standards__ratio">x0.5</span>
                            </td>
                      <td>
                90                                  <span class="standards__ratio">x0.75</span>
                            </td>
                      <td>
                125                                  <span class="standards__ratio">x1.04</span>
                            </td>
                      <td>
                163                                  <span class="standards__ratio">x1.36</span>
                            </td>
                </tr>
          <tr>
                      <td>
                130                                  <span class="standards__ratio"></span>
                            </td>
                      <td>
                44                                  <span class="standards__ratio">x0.34</span>
                            </td>
                      <td>
                68                                  <span class="standards__ratio">x0.52</span>
                            </td>
                      <td>
                99                                  <span class="standards__ratio">x0.76</span>
                            </td>
                      <td>
                136                                  <span class="standards__ratio">x1.04</span>
                            </td>
                      <td>
                176                                  <span class="standards__ratio">x1.35</span>
                            </td>
                </tr>
          <tr>
                      <td>
                140                                  <span class="standards__ratio"></span>
                            </td>
                      <td>
                50                                  <span class="standards__ratio">x0.36</span>
                            </td>
                      <td>
                76                                  <span class="standards__ratio">x0.54</span>
                            </td>
                      <td>
                108                                  <span class="standards__ratio">x0.77</span>
                            </td>
                      <td>
                146                                  <span class="standards__ratio">x1.04</span>
                            </td>
                      <td>
                188                                  <span class="standards__ratio">x1.34</span>
                            </td>
                </tr>
          <tr>
                      <td>
                150                                  <span class="standards__ratio"></span>
                            </td>
                      <td>
                56                                  <span class="standards__ratio">x0.37</span>
                            </td>
                      <td>
                83                                  <span class="standards__ratio">x0.55</span>
                            </td>
                      <td>
                117                                  <span class="standards__ratio">x0.78</span>
                            </td>
                      <td>
                156                                  <span class="standards__ratio">x1.04</span>
                            </td>
                      <td>
                199                                  <span class="standards__ratio">x1.33</span>
                            </td>
                </tr>
          <tr>
                      <td>
                160                                  <span class="standards__ratio"></span>
                            </td>
                      <td>
                62                                  <span class="standards__ratio">x0.39</span>
                            </td>
                      <td>
                90                                  <span class="standards__ratio">x0.56</span>
                            </td>
                      <td>
                125                                  <span class="standards__ratio">x0.78</span>
                            </td>
                      <td>
                166                                  <span class="standards__ratio">x1.04</span>
                            </td>
                      <td>
                210                                  <span class="standards__ratio">x1.32</span>
                            </td>
                </tr>
          <tr>
                      <td>
                170                                  <span class="standards__ratio"></span>
                            </td>
                      <td>
                68                                  <span class="standards__ratio">x0.4</span>
                            </td>
                      <td>
                97                                  <span class="standards__ratio">x0.57</span>
                            </td>
                      <td>
                134                                  <span class="standards__ratio">x0.79</span>
                            </td>
                      <td>
                176                                  <span class="standards__ratio">x1.03</span>
                            </td>
                      <td>
                221                                  <span class="standards__ratio">x1.3</span>
                            </td>
                </tr>
          <tr>
                      <td>
                180                                  <span class="standards__ratio"></span>
                            </td>
                      <td>
                74                                  <span class="standards__ratio">x0.41</span>
                            </td>
                      <td>
                104                                  <span class="standards__ratio">x0.58</span>
                            </td>
                      <td>
                142                                  <span class="standards__ratio">x0.79</span>
                            </td>
                      <td>
                185                                  <span class="standards__ratio">x1.03</span>
                            </td>
                      <td>
                231                                  <span class="standards__ratio">x1.29</span>
                            </td>
                </tr>
          <tr>
                      <td>
                190                                  <span class="standards__ratio"></span>
                            </td>
                      <td>
                79                                  <span class="standards__ratio">x0.42</span>
                            </td>
                      <td>
                111                                  <span class="standards__ratio">x0.58</span>
                            </td>
                      <td>
                150                                  <span class="standards__ratio">x0.79</span>
                            </td>
                      <td>
                194                                  <span class="standards__ratio">x1.02</span>
                            </td>
                      <td>
                241                                  <span class="standards__ratio">x1.27</span>
                            </td>
                </tr>
          <tr>
                      <td>
                200                                  <span class="standards__ratio"></span>
                            </td>
                      <td>
                85                                  <span class="standards__ratio">x0.43</span>
                            </td>
                      <td>
                118                                  <span class="standards__ratio">x0.59</span>
                            </td>
                      <td>
                157                                  <span class="standards__ratio">x0.79</span>
                            </td>
                      <td>
                202                                  <span class="standards__ratio">x1.01</span>
                            </td>
                      <td>
                251                                  <span class="standards__ratio">x1.26</span>
                            </td>
                </tr>
          <tr>
                      <td>
                210                                  <span class="standards__ratio"></span>
                            </td>
                      <td>
                91                                  <span class="standards__ratio">x0.43</span>
                            </td>
                      <td>
                124                                  <span class="standards__ratio">x0.59</span>
                            </td>
                      <td>
                165                                  <span class="standards__ratio">x0.78</span>
                            </td>
                      <td>
                211                                  <span class="standards__ratio">x1</span>
                            </td>
                      <td>
                260                                  <span class="standards__ratio">x1.24</span>
                            </td>
                </tr>
          <tr>
                      <td>
                220                                  <span class="standards__ratio"></span>
                            </td>
                      <td>
                96                                  <span class="standards__ratio">x0.44</span>
                            </td>
                      <td>
                130                                  <span class="standards__ratio">x0.59</span>
                            </td>
                      <td>
                172                                  <span class="standards__ratio">x0.78</span>
                            </td>
                      <td>
                219                                  <span class="standards__ratio">x1</span>
                            </td>
                      <td>
                270                                  <span class="standards__ratio">x1.23</span>
                            </td>
                </tr>
          <tr>
                      <td>
                230                                  <span class="standards__ratio"></span>
                            </td>
                      <td>
                101                                  <span class="standards__ratio">x0.44</span>
                            </td>
                      <td>
                136                                  <span class="standards__ratio">x0.59</span>
                            </td>
                      <td>
                179                                  <span class="standards__ratio">x0.78</span>
                            </td>
                      <td>
                227                                  <span class="standards__ratio">x0.99</span>
                            </td>
                      <td>
                278                                  <span class="standards__ratio">x1.21</span>
                            </td>
                </tr>
          <tr>
                      <td>
                240                                  <span class="standards__ratio"></span>
                            </td>
                      <td>
                106                                  <span class="standards__ratio">x0.44</span>
                            </td>
                      <td>
                142                                  <span class="standards__ratio">x0.59</span>
                            </td>
                      <td>
                186                                  <span class="standards__ratio">x0.77</span>
                            </td>
                      <td>
                235                                  <span class="standards__ratio">x0.98</span>
                            </td>
                      <td>
                287                                  <span class="standards__ratio">x1.2</span>
                            </td>
                </tr>
          <tr>
                      <td>
                250                                  <span class="standards__ratio"></span>
                            </td>
                      <td>
                112                                  <span class="standards__ratio">x0.45</span>
                            </td>
                      <td>
                148                                  <span class="standards__ratio">x0.59</span>
                            </td>
                      <td>
                193                                  <span class="standards__ratio">x0.77</span>
                            </td>
                      <td>
                242                                  <span class="standards__ratio">x0.97</span>
                            </td>
                      <td>
                295                                  <span class="standards__ratio">x1.18</span>
                            </td>
                </tr>
          <tr>
                      <td>
                260                                  <span class="standards__ratio"></span>
                            </td>
                      <td>
                117                                  <span class="standards__ratio">x0.45</span>
                            </td>
                      <td>
                154                                  <span class="standards__ratio">x0.59</span>
                            </td>
                      <td>
                199                                  <span class="standards__ratio">x0.77</span>
                            </td>
                      <td>
                250                                  <span class="standards__ratio">x0.96</span>
                            </td>
                      <td>
                304                                  <span class="standards__ratio">x1.17</span>
                            </td>
                </tr>
          <tr>
                      <td>
                270                                  <span class="standards__ratio"></span>
                            </td>
                      <td>
                122                                  <span class="standards__ratio">x0.45</span>
                            </td>
                      <td>
                160                                  <span class="standards__ratio">x0.59</span>
                            </td>
                      <td>
                206                                  <span class="standards__ratio">x0.76</span>
                            </td>
                      <td>
                257                                  <span class="standards__ratio">x0.95</span>
                            </td>
                      <td>
                311                                  <span class="standards__ratio">x1.15</span>
                            </td>
                </tr>
          <tr>
                      <td>
                280                                  <span class="standards__ratio"></span>
                            </td>
                      <td>
                126                                  <span class="standards__ratio">x0.45</span>
                            </td>
                      <td>
                165                                  <span class="standards__ratio">x0.59</span>
                            </td>
                      <td>
                212                                  <span class="standards__ratio">x0.76</span>
                            </td>
                      <td>
                264                                  <span class="standards__ratio">x0.94</span>
                            </td>
                      <td>
                319                                  <span class="standards__ratio">x1.14</span>
                            </td>
                </tr>
          <tr>
                      <td>
                290                                  <span class="standards__ratio"></span>
                            </td>
                      <td>
                131                                  <span class="standards__ratio">x0.45</span>
                            </td>
                      <td>
                171                                  <span class="standards__ratio">x0.59</span>
                            </td>
                      <td>
                218                                  <span class="standards__ratio">x0.75</span>
                            </td>
                      <td>
                271                                  <span class="standards__ratio">x0.93</span>
                            </td>
                      <td>
                327                                  <span class="standards__ratio">x1.13</span>
                            </td>
                </tr>
          <tr>
                      <td>
                300                                  <span class="standards__ratio"></span>
                            </td>
                      <td>
                136                                  <span class="standards__ratio">x0.45</span>
                            </td>
                      <td>
                176                                  <span class="standards__ratio">x0.59</span>
                            </td>
                      <td>
                224                                  <span class="standards__ratio">x0.75</span>
                            </td>
                      <td>
                278                                  <span class="standards__ratio">x0.93</span>
                            </td>
                      <td>
                334                                  <span class="standards__ratio">x1.11</span>
                            </td>
                </tr>
          <tr>
                      <td>
                310                                  <span class="standards__ratio"></span>
                            </td>
                      <td>
                141                                  <span class="standards__ratio">x0.45</span>
                            </td>
                      <td>
                182                                  <span class="standards__ratio">x0.59</span>
                            </td>
                      <td>
                230                                  <span class="standards__ratio">x0.74</span>
                            </td>
                      <td>
                284                                  <span class="standards__ratio">x0.92</span>
                            </td>
                      <td>
                341                                  <span class="standards__ratio">x1.1</span>
                            </td>
                </tr>
        </tbody>
              <tfoot class="standards__footer">
        <tr>
                          <td>
                  All              </td>
                          <td>
                  65              </td>
                          <td>
                  98              </td>
                          <td>
                  139              </td>
                          <td>
                  188              </td>
                          <td>
                  242              </td>
                    </tr>
        </tfoot>
        </table>
        <table class="table is-fullwidth is-striped is-narrow standards__table" id="tableId601a1d965b0a0">
        <thead>
        <tr>
          <th style="width: 25%;">
            <abbr title="Bodyweight"><span>BW</span></abbr>
          </th>
                      <th style="width: 15%;">
                <abbr
                    title="Beginner"><span>Beg.</span></abbr>
              </th>
                      <th style="width: 15%;">
                <abbr
                    title="Novice"><span>Nov.</span></abbr>
              </th>
                      <th style="width: 15%;">
                <abbr
                    title="Intermediate"><span>Int.</span></abbr>
              </th>
                      <th style="width: 15%;">
                <abbr
                    title="Advanced"><span>Adv.</span></abbr>
              </th>
                      <th style="width: 15%;">
                <abbr
                    title="Elite"><span>Elite</span></abbr>
              </th>
                </tr>
        </thead>
        <tbody>
              <tr>
                          <td>
                    90                                  <span class="standards__ratio"></span>
                                </td>
                          <td>
                    17                                  <span class="standards__ratio">x0.19</span>
                                </td>
                          <td>
                    31                                  <span class="standards__ratio">x0.35</span>
                                </td>
                          <td>
                    51                                  <span class="standards__ratio">x0.57</span>
                                </td>
                          <td>
                    76                                  <span class="standards__ratio">x0.85</span>
                                </td>
                          <td>
                    104                                  <span class="standards__ratio">x1.16</span>
                                </td>
                    </tr>
              <tr>
                          <td>
                    100                                  <span class="standards__ratio"></span>
                                </td>
                          <td>
                    20                                  <span class="standards__ratio">x0.2</span>
                                </td>
                          <td>
                    35                                  <span class="standards__ratio">x0.35</span>
                                </td>
                          <td>
                    56                                  <span class="standards__ratio">x0.56</span>
                                </td>
                          <td>
                    82                                  <span class="standards__ratio">x0.82</span>
                                </td>
                          <td>
                    111                                  <span class="standards__ratio">x1.11</span>
                                </td>
                    </tr>
              <tr>
                          <td>
                    110                                  <span class="standards__ratio"></span>
                                </td>
                          <td>
                    22                                  <span class="standards__ratio">x0.2</span>
                                </td>
                          <td>
                    38                                  <span class="standards__ratio">x0.35</span>
                                </td>
                          <td>
                    60                                  <span class="standards__ratio">x0.55</span>
                                </td>
                          <td>
                    87                                  <span class="standards__ratio">x0.79</span>
                                </td>
                          <td>
                    117                                  <span class="standards__ratio">x1.07</span>
                                </td>
                    </tr>
              <tr>
                          <td>
                    120                                  <span class="standards__ratio"></span>
                                </td>
                          <td>
                    25                                  <span class="standards__ratio">x0.21</span>
                                </td>
                          <td>
                    42                                  <span class="standards__ratio">x0.35</span>
                                </td>
                          <td>
                    65                                  <span class="standards__ratio">x0.54</span>
                                </td>
                          <td>
                    92                                  <span class="standards__ratio">x0.77</span>
                                </td>
                          <td>
                    123                                  <span class="standards__ratio">x1.03</span>
                                </td>
                    </tr>
              <tr>
                          <td>
                    130                                  <span class="standards__ratio"></span>
                                </td>
                          <td>
                    27                                  <span class="standards__ratio">x0.21</span>
                                </td>
                          <td>
                    45                                  <span class="standards__ratio">x0.35</span>
                                </td>
                          <td>
                    68                                  <span class="standards__ratio">x0.53</span>
                                </td>
                          <td>
                    97                                  <span class="standards__ratio">x0.74</span>
                                </td>
                          <td>
                    128                                  <span class="standards__ratio">x0.99</span>
                                </td>
                    </tr>
              <tr>
                          <td>
                    140                                  <span class="standards__ratio"></span>
                                </td>
                          <td>
                    30                                  <span class="standards__ratio">x0.21</span>
                                </td>
                          <td>
                    48                                  <span class="standards__ratio">x0.34</span>
                                </td>
                          <td>
                    72                                  <span class="standards__ratio">x0.52</span>
                                </td>
                          <td>
                    101                                  <span class="standards__ratio">x0.72</span>
                                </td>
                          <td>
                    134                                  <span class="standards__ratio">x0.95</span>
                                </td>
                    </tr>
              <tr>
                          <td>
                    150                                  <span class="standards__ratio"></span>
                                </td>
                          <td>
                    32                                  <span class="standards__ratio">x0.21</span>
                                </td>
                          <td>
                    51                                  <span class="standards__ratio">x0.34</span>
                                </td>
                          <td>
                    76                                  <span class="standards__ratio">x0.51</span>
                                </td>
                          <td>
                    106                                  <span class="standards__ratio">x0.7</span>
                                </td>
                          <td>
                    139                                  <span class="standards__ratio">x0.92</span>
                                </td>
                    </tr>
              <tr>
                          <td>
                    160                                  <span class="standards__ratio"></span>
                                </td>
                          <td>
                    34                                  <span class="standards__ratio">x0.21</span>
                                </td>
                          <td>
                    54                                  <span class="standards__ratio">x0.34</span>
                                </td>
                          <td>
                    79                                  <span class="standards__ratio">x0.5</span>
                                </td>
                          <td>
                    110                                  <span class="standards__ratio">x0.69</span>
                                </td>
                          <td>
                    143                                  <span class="standards__ratio">x0.89</span>
                                </td>
                    </tr>
              <tr>
                          <td>
                    170                                  <span class="standards__ratio"></span>
                                </td>
                          <td>
                    36                                  <span class="standards__ratio">x0.21</span>
                                </td>
                          <td>
                    57                                  <span class="standards__ratio">x0.33</span>
                                </td>
                          <td>
                    83                                  <span class="standards__ratio">x0.49</span>
                                </td>
                          <td>
                    114                                  <span class="standards__ratio">x0.67</span>
                                </td>
                          <td>
                    148                                  <span class="standards__ratio">x0.87</span>
                                </td>
                    </tr>
              <tr>
                          <td>
                    180                                  <span class="standards__ratio"></span>
                                </td>
                          <td>
                    39                                  <span class="standards__ratio">x0.21</span>
                                </td>
                          <td>
                    59                                  <span class="standards__ratio">x0.33</span>
                                </td>
                          <td>
                    86                                  <span class="standards__ratio">x0.48</span>
                                </td>
                          <td>
                    117                                  <span class="standards__ratio">x0.65</span>
                                </td>
                          <td>
                    152                                  <span class="standards__ratio">x0.84</span>
                                </td>
                    </tr>
              <tr>
                          <td>
                    190                                  <span class="standards__ratio"></span>
                                </td>
                          <td>
                    41                                  <span class="standards__ratio">x0.21</span>
                                </td>
                          <td>
                    62                                  <span class="standards__ratio">x0.33</span>
                                </td>
                          <td>
                    89                                  <span class="standards__ratio">x0.47</span>
                                </td>
                          <td>
                    121                                  <span class="standards__ratio">x0.64</span>
                                </td>
                          <td>
                    156                                  <span class="standards__ratio">x0.82</span>
                                </td>
                    </tr>
              <tr>
                          <td>
                    200                                  <span class="standards__ratio"></span>
                                </td>
                          <td>
                    43                                  <span class="standards__ratio">x0.21</span>
                                </td>
                          <td>
                    64                                  <span class="standards__ratio">x0.32</span>
                                </td>
                          <td>
                    92                                  <span class="standards__ratio">x0.46</span>
                                </td>
                          <td>
                    124                                  <span class="standards__ratio">x0.62</span>
                                </td>
                          <td>
                    160                                  <span class="standards__ratio">x0.8</span>
                                </td>
                    </tr>
              <tr>
                          <td>
                    210                                  <span class="standards__ratio"></span>
                                </td>
                          <td>
                    45                                  <span class="standards__ratio">x0.21</span>
                                </td>
                          <td>
                    67                                  <span class="standards__ratio">x0.32</span>
                                </td>
                          <td>
                    95                                  <span class="standards__ratio">x0.45</span>
                                </td>
                          <td>
                    128                                  <span class="standards__ratio">x0.61</span>
                                </td>
                          <td>
                    164                                  <span class="standards__ratio">x0.78</span>
                                </td>
                    </tr>
              <tr>
                          <td>
                    220                                  <span class="standards__ratio"></span>
                                </td>
                          <td>
                    46                                  <span class="standards__ratio">x0.21</span>
                                </td>
                          <td>
                    69                                  <span class="standards__ratio">x0.31</span>
                                </td>
                          <td>
                    98                                  <span class="standards__ratio">x0.44</span>
                                </td>
                          <td>
                    131                                  <span class="standards__ratio">x0.59</span>
                                </td>
                          <td>
                    167                                  <span class="standards__ratio">x0.76</span>
                                </td>
                    </tr>
              <tr>
                          <td>
                    230                                  <span class="standards__ratio"></span>
                                </td>
                          <td>
                    48                                  <span class="standards__ratio">x0.21</span>
                                </td>
                          <td>
                    71                                  <span class="standards__ratio">x0.31</span>
                                </td>
                          <td>
                    100                                  <span class="standards__ratio">x0.44</span>
                                </td>
                          <td>
                    134                                  <span class="standards__ratio">x0.58</span>
                                </td>
                          <td>
                    171                                  <span class="standards__ratio">x0.74</span>
                                </td>
                    </tr>
              <tr>
                          <td>
                    240                                  <span class="standards__ratio"></span>
                                </td>
                          <td>
                    50                                  <span class="standards__ratio">x0.21</span>
                                </td>
                          <td>
                    74                                  <span class="standards__ratio">x0.31</span>
                                </td>
                          <td>
                    103                                  <span class="standards__ratio">x0.43</span>
                                </td>
                          <td>
                    137                                  <span class="standards__ratio">x0.57</span>
                                </td>
                          <td>
                    174                                  <span class="standards__ratio">x0.73</span>
                                </td>
                    </tr>
              <tr>
                          <td>
                    250                                  <span class="standards__ratio"></span>
                                </td>
                          <td>
                    52                                  <span class="standards__ratio">x0.21</span>
                                </td>
                          <td>
                    76                                  <span class="standards__ratio">x0.3</span>
                                </td>
                          <td>
                    105                                  <span class="standards__ratio">x0.42</span>
                                </td>
                          <td>
                    140                                  <span class="standards__ratio">x0.56</span>
                                </td>
                          <td>
                    177                                  <span class="standards__ratio">x0.71</span>
                                </td>
                    </tr>
              <tr>
                          <td>
                    260                                  <span class="standards__ratio"></span>
                                </td>
                          <td>
                    54                                  <span class="standards__ratio">x0.21</span>
                                </td>
                          <td>
                    78                                  <span class="standards__ratio">x0.3</span>
                                </td>
                          <td>
                    108                                  <span class="standards__ratio">x0.41</span>
                                </td>
                          <td>
                    143                                  <span class="standards__ratio">x0.55</span>
                                </td>
                          <td>
                    181                                  <span class="standards__ratio">x0.69</span>
                                </td>
                    </tr>
            </tbody>
                  <tfoot class="standards__footer">
            <tr>
                              <td>
                      All              </td>
                              <td>
                      29              </td>
                              <td>
                      48              </td>
                              <td>
                      74              </td>
                              <td>
                      105              </td>
                              <td>
                      140              </td>
                        </tr>
            </tfoot>
            </table>
    `
}