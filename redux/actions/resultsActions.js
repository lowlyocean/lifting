// Set Inputs
export const setResults = (value) => ({
    type: 'SET_RESULTS',
    results: value
  });