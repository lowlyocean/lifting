// Imports: Dependencies
import { combineReducers } from 'redux';
// Imports: Reducers
import inputsReducer from './inputsReducer';
import calculatedReducer from './calculatedReducer';
import resultsReducer from './resultsReducer';

// Redux: Root Reducer
const rootReducer = combineReducers({
  inputsReducer: inputsReducer,
  calculatedReducer: calculatedReducer,
  resultsReducer: resultsReducer
});
// Exports
export default rootReducer;