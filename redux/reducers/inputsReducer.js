// Initial State
const initialState = {
  inputs: {
    gender: 'M',
    body_weight: 180,
    reps: 8,
    start_date: new Date('Dec 26 2020 00:00:00 EST'),
    workout_date: new Date()
  }
}

   // Reducers (Modifies The State And Returns A New State)
   const inputsReducer = (state = initialState, action) => {
    switch (action.type) {
      // Set inputs
      case 'SET_INPUTS': {
        return {
          // State
          ...state,
          // Redux Store
          inputs: action.inputs,
        }
      }
      // Default
      default: {
        return state;
      }
    }
  };
  // Exports
  export default inputsReducer;