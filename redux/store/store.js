// Imports: Dependencies
import AsyncStorage from '@react-native-async-storage/async-storage';
import { createStore, /*applyMiddleware*/ } from 'redux';
// import { createLogger } from 'redux-logger';
import { persistStore, persistReducer, createTransform } from 'redux-persist';
// Imports: Redux
import rootReducer from '../reducers/index';
// Middleware: Redux Persist Config
const persistConfig = {
  // Root
  key: 'root',
    // Transform dates back into JS Dates on rehydrate
  // (see: https://github.com/rt2zz/redux-persist/issues/82)
  transforms: [
    createTransform(JSON.stringify, (toRehydrate) =>
      JSON.parse(toRehydrate, (key, value) =>
        typeof value === 'string' && value.match(/^\d{4}-\d{2}-\d{2}T\d{2}:\d{2}:\d{2}/)
          ? new Date(value)
          : value,
      ),
    ),
  ],
  // Storage Method (React Native)
  storage: AsyncStorage,
  // Whitelist (Save Specific Reducers)
  whitelist: [
    'inputsReducer',
  ],
  // Blacklist (Don't Save Specific Reducers)
  blacklist: [
    'calculatedReducer',
    'resultsReducer'
  ],
};
// Middleware: Redux Persist Persisted Reducer
const persistedReducer = persistReducer(persistConfig, rootReducer);
// Redux: Store
const store = createStore(
  persistedReducer,
  // applyMiddleware(
  //   createLogger(),
  // ),
);
// Middleware: Redux Persist Persister
let persistor = persistStore(store);
// Exports
export {
  store,
  persistor,
};